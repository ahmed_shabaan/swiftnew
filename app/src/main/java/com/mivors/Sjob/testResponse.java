package com.mivors.Sjob;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

public class testResponse {

    @SerializedName("items")
    public List<Items> items;
    @SerializedName("links")
    public List<Links> links;

    public static class Items {
        @SerializedName("tableName")
        public String tableName;
        @SerializedName("count")
        public int count;
    }

    public static class Links {
        @SerializedName("rel")
        public String rel;
        @SerializedName("href")
        public String href;
    }
}
