package com.mivors.Sjob.RightNow.RNClasses.Contact;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public interface ContactCallBacks {
     void CreateContact(String result, int code);
     void UpdateContact(String result, int code);
     void GetContactByID(String result, int code);
     void GetContactQueryResult(String result, int code);

}
