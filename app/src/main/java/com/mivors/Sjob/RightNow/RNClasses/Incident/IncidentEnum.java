package com.mivors.Sjob.RightNow.RNClasses.Incident;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public enum IncidentEnum {

    primaryContact("primaryContact"),
    subject("subject") ;

    private final String text;


    private IncidentEnum(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

}
