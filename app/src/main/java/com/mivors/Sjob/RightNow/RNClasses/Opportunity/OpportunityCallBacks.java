package com.mivors.Sjob.RightNow.RNClasses.Opportunity;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public interface OpportunityCallBacks {
    void CreateOpportunity(String result, int code);
    void UpdateOpportunity(String result, int code);
    void GetOpportunityByID(String result, int code);
    void GetOpportunityQueryResult(String result, int code);
}
