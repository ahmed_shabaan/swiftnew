package com.mivors.Sjob.RightNow.RNClasses.Contact;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public enum ContactEnum {



    name("name"),
    emails("emails") ;

    private final String text;


    private ContactEnum(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

}
