package com.mivors.Sjob.RightNow.ApiClient;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by ahmed shabaan on 6/15/2016.
 */
public interface ApiInterface {



    // contact
    @POST("{path}")
    Call<String> Create(@Path("path") String RnName, @Body String body, @Header("Authorization") String auth, @Header("Content-Type") String content_type);
    @POST
    Call<String> forget(@Url  String RnName, @Body String body, @Header("Authorization") String auth, @Header("Content-Type") String content_type);
//    @Multipart
//    @POST
//    Call<String> myPlans(@Url  String RnName, @Part(Constants.ACTION_ID) RequestBody actionId, @Part(Constants.OFFER_CODE) RequestBody offerCode);

    @FormUrlEncoded
    @POST
    Call<String>  forgetPass(@Url String url,@Field("contact_mail") String email);

    // Contact
    @POST("{path1}/{path2}")
    Call<String> Update(@Path("path1") String RnName, @Path("path2") int path, @Body String body, @Header("Authorization") String auth, @Header("Content-Type") String content_type, @Header("X-HTTP-Method-Override  ") String patch);


    //get any by id
    @GET("{path1}/{path2}")
    Call<String> GetAnyByID(@Path("path1") String RnName, @Path("path2") int path, @Header("Authorization") String auth, @Header("Content-Type") String content_type);


    // get all using queries
    @GET("queryResults")
    Call<String> GetAllUsingQuery(@Query("query") String path, @Header("Authorization") String auth, @Header("Content-Type") String content_type);





}
