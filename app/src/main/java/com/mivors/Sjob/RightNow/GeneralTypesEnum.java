package com.mivors.Sjob.RightNow;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public enum GeneralTypesEnum {


    contacts ("contacts"),
    opportunities ("opportunities"),
    incidents ("incidents");

    private final String name;

    private GeneralTypesEnum(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
