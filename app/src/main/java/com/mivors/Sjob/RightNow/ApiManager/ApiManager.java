package com.mivors.Sjob.RightNow.ApiManager;


import android.content.Context;
import android.util.Log;

import com.mivors.Sjob.RightNow.ApiClient.ApiInterface;
import com.mivors.Sjob.RightNow.ApiClient.ServicesConnection;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.RightNow.RNClasses.Incident.IncidentCallBacks;
import com.mivors.Sjob.RightNow.RNClasses.Opportunity.OpportunityCallBacks;
import com.mivors.Sjob.RightNow.RNObject;
import com.mivors.Sjob.RightNow.RequestCodes;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public class ApiManager {

    IncidentCallBacks IncidentCallBacks;
    ContactCallBacks ContactCallBacks;
    OpportunityCallBacks OpportunityCallBacks;

    @Inject
    ApiInterface apiInterface;

    public ApiManager(Context context) {
        ((DaggerApplication) context).getAppComponent().inject(this);
    }

    public void RegisterIncidentCallback(IncidentCallBacks IncidentCallBacks) {
        this.IncidentCallBacks = IncidentCallBacks;
    }

    public void RegisterContactCallback(ContactCallBacks ContactCallBacks) {
        this.ContactCallBacks = ContactCallBacks;
    }

    public void RegisterOpportunityCallback(OpportunityCallBacks OpportunityCallBacks) {
        this.OpportunityCallBacks = OpportunityCallBacks;
    }


    public void Create(RNObject RightNowObject, String data) {
        HashMap<String, Object> hashMap = RightNowObject.getFields();
        JSONObject request = new JSONObject();
        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.incidents.toString())) {

            if (hashMap != null) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    try {
                        request.put(key, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


                Call<String> NewIncidentCall = ServicesConnection.GetService().Create(GeneralTypesEnum.incidents.toString(), data, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE);
                NewIncidentCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            IncidentCallBacks.CreateIncident(response.body(), response.code());
                        } else {
                            IncidentCallBacks.CreateIncident(response.raw().networkResponse().toString(), response.code());

                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        IncidentCallBacks.CreateIncident(t.toString(), RequestCodes.InterNetError);
                    }
                });
            }
        }

        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.contacts.toString())) {

            if (hashMap != null) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    try {
                        request.put(key, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                Call<String> NewIncidentCall = ServicesConnection.GetService().Create(GeneralTypesEnum.contacts.toString(), data, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE);
                NewIncidentCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            ContactCallBacks.CreateContact(response.body(), response.code());
                        } else {
                            ContactCallBacks.CreateContact(response.raw().networkResponse().toString(), response.code());

                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        ContactCallBacks.CreateContact(t.toString(), RequestCodes.InterNetError);
                    }
                });
            }
        }

        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.opportunities.toString())) {

            if (hashMap != null) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    try {
                        request.put(key, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                String Data = request.toString();
                Call<String> NewIncidentCall = ServicesConnection.GetService().Create(GeneralTypesEnum.opportunities.toString(), Data, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE);
                NewIncidentCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            OpportunityCallBacks.CreateOpportunity(response.body(), response.code());
                        } else {
                            OpportunityCallBacks.CreateOpportunity(response.raw().networkResponse().toString(), response.code());

                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                        OpportunityCallBacks.CreateOpportunity(t.toString(), RequestCodes.InterNetError);
                    }
                });
            }
        }

    }


    public void forgetPass(RNObject RightNowObject, String data) {
        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.contacts.toString())) {

            Call<String> NewIncidentCall = ServicesConnection.GetService().forgetPass(Utility.FORGET_PASS,data);
            NewIncidentCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        try {
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ContactCallBacks.CreateContact(response.body(), response.code());
                    } else {
                        ContactCallBacks.CreateContact(response.raw().networkResponse().toString(), response.code());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    ContactCallBacks.CreateContact(t.toString(), RequestCodes.InterNetError);
                }
            });
        }


    }


    public void Update(RNObject RightNowObject, String data) {
        HashMap<String, Object> hashMap = RightNowObject.getFields();
        JSONObject request = new JSONObject();
        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.incidents.toString())) {

            if (hashMap != null) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    try {
                        request.put(key, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Call<String> UpdateIncidentCall = ServicesConnection.GetService().Update(GeneralTypesEnum.incidents.toString(), RightNowObject.getId(), data, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE, ServicesConnection.PATCH);
                UpdateIncidentCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            IncidentCallBacks.UpdateIncident("", response.code());
                        } else {
                            IncidentCallBacks.UpdateIncident("", response.code());
                            if (response.isSuccessful()) {

                                IncidentCallBacks.UpdateIncident("", response.code());
                            } else {

                                IncidentCallBacks.UpdateIncident("", response.code());

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                        IncidentCallBacks.UpdateIncident(t.toString(), RequestCodes.InterNetError);
                    }
                });
            }
        }


        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.contacts.toString())) {

            if (hashMap != null) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    try {
                        request.put(key, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Call<String> UpdateIncidentCall = ServicesConnection.GetService().Update(GeneralTypesEnum.contacts.toString(), RightNowObject.getId(), data, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE, ServicesConnection.PATCH);
                UpdateIncidentCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            System.out.println(response);

                            ContactCallBacks.UpdateContact(response.body(), response.code());
                        } else {
                            System.out.println(response);

                            ContactCallBacks.UpdateContact(response.raw().networkResponse().toString(), response.code());
                            if (response.isSuccessful()) {

                                ContactCallBacks.UpdateContact(response.body(), response.code());
                            } else {

                                ContactCallBacks.UpdateContact(response.raw().networkResponse().toString(), response.code());

                            }

                        }
                    }
                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                        System.out.println(t.toString());
                        ContactCallBacks.UpdateContact(t.toString(), RequestCodes.InterNetError);
                    }
                });
            }
        }


        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.opportunities.toString())) {

            if (hashMap != null) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    try {
                        request.put(key, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                String Data = request.toString();
                Call<String> UpdateIncidentCall = ServicesConnection.GetService().Update(GeneralTypesEnum.opportunities.toString(), RightNowObject.getId(), Data, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE, ServicesConnection.PATCH);
                UpdateIncidentCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            OpportunityCallBacks.UpdateOpportunity(response.body(), response.code());
                            System.out.println(response);
                        } else {
                            System.out.println(response);
                            OpportunityCallBacks.UpdateOpportunity(response.raw().networkResponse().toString(), response.code());
                            if (response.isSuccessful()) {
                                OpportunityCallBacks.UpdateOpportunity(response.body(), response.code());

                            } else {

                                OpportunityCallBacks.UpdateOpportunity(response.raw().networkResponse().toString(), response.code());

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                        System.out.println(t.toString());
                        OpportunityCallBacks.UpdateOpportunity(t.toString(), RequestCodes.InterNetError);
                    }
                });
            }
        }


    }


    public void GetById(RNObject RightNowObject) {
        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.incidents.toString())) {
            Call<String> GetIncidentByIdCall = ServicesConnection.GetService().GetAnyByID(GeneralTypesEnum.incidents.toString(), RightNowObject.getId(), ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE);
            GetIncidentByIdCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        IncidentCallBacks.GetIncidentByID(response.body(), response.code());
                    } else {
                        IncidentCallBacks.GetIncidentByID(response.raw().networkResponse().toString(), response.code());

                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IncidentCallBacks.GetIncidentByID(t.toString(), RequestCodes.InterNetError);
                }
            });

        }


        if (RightNowObject.getObjectName().equals(GeneralTypesEnum.opportunities.toString())) {
            Call<String> GetIncidentByIdCall = ServicesConnection.GetService().GetAnyByID(GeneralTypesEnum.opportunities.toString(), RightNowObject.getId(), ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE);
            GetIncidentByIdCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        OpportunityCallBacks.GetOpportunityByID(response.body(), response.code());
                    } else {
                        OpportunityCallBacks.GetOpportunityByID(response.raw().networkResponse().toString(), response.code());

                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    OpportunityCallBacks.GetOpportunityByID(t.toString(), RequestCodes.InterNetError);
                }
            });

        }

    }

    public void GetResources(RNObject RightNowObject) {
        // RightNowObject.getObjectName().equals()

    }

    public void GetQueryResult(String Query, final String name) {


        Call<String> QueryCall = apiInterface.GetAllUsingQuery(Query, ServicesConnection.getBasicAuthentication(), ServicesConnection.CONTENT_TYPE);
        QueryCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (name.equals(GeneralTypesEnum.incidents.name())) {
                        IncidentCallBacks.GetIncidentQueryResult(response.body(), response.code());

                    } else if (name.equals(GeneralTypesEnum.contacts.name())) {
                        ContactCallBacks.GetContactQueryResult(response.body(), response.code());

                }else if (name.equals(GeneralTypesEnum.opportunities.name())) {
                        OpportunityCallBacks.GetOpportunityQueryResult(response.body(), response.code());

                    }
                } else {

                    if (name.equals(GeneralTypesEnum.incidents.name())) {
                        IncidentCallBacks.GetIncidentQueryResult(response.raw().networkResponse().toString(), response.code());

                    } else if (name.equals(GeneralTypesEnum.contacts.name())) {
                        ContactCallBacks.GetContactQueryResult(response.raw().networkResponse().toString(), response.code());

                    }else if (name.equals(GeneralTypesEnum.opportunities.name())) {
                        OpportunityCallBacks.GetOpportunityQueryResult(response.raw().networkResponse().toString(), response.code());
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (name.equals(GeneralTypesEnum.incidents.name())) {
                    IncidentCallBacks.GetIncidentQueryResult(t.toString(), RequestCodes.InterNetError);

                } else if (name.equals(GeneralTypesEnum.contacts.name())) {
                    ContactCallBacks.GetContactQueryResult(t.toString(), RequestCodes.InterNetError);

                }else if (name.equals(GeneralTypesEnum.opportunities.name())) {
                    OpportunityCallBacks.GetOpportunityQueryResult(t.toString(), RequestCodes.InterNetError);
                }
            }
        });


    }


}
