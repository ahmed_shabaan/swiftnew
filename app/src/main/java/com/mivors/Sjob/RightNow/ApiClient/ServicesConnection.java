package com.mivors.Sjob.RightNow.ApiClient;


import android.util.Base64;

/**
 * Created by Yehia Fathi on 11/15/2016.
 */
public class ServicesConnection {

    private static ApiInterface apiInterface = null;
    public static final String PATCH = "PATCH";
    public static final String CONTENT_TYPE = "application/json";
    private static final String username = "ahmed_bahaa";
    private static final String password = "Mivors@2016";
    public static final String BASE_URL = "https://swiftcare.custhelp.com/services/rest/connect/v1.3/";



    private ServicesConnection(){}

    public static ApiInterface GetService(){
        if(apiInterface==null){
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
        }

        return  apiInterface;
    }

    public static String  getBasicAuthentication(){
        return   "Basic " + Base64.encodeToString(
                (username + ":" + password).getBytes(),
                Base64.NO_WRAP);

    }



}
