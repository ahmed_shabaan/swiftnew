package com.mivors.Sjob.RightNow;

import java.util.HashMap;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public class RNObject {
    private  HashMap<String,Object>  fields  = new HashMap<>() ;
    private  String ObjectName;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RNObject(String objectName) {
        ObjectName = objectName;
    }

    public String getObjectName() {
        return ObjectName;
    }

    public void setObjectName(String objectName) {
        ObjectName = objectName;
    }

    public void setField(String key, Object value) {
        fields.put(key,value);
    }

    public HashMap<String, Object> getFields() {
        return fields;
    }
}
