package com.mivors.Sjob.RightNow.RNClasses.Incident;

/**
 * Created by Ahmed shaban on 5/10/2017.
 */

public interface IncidentCallBacks {
    void CreateIncident(String result, int code);
    void UpdateIncident(String result, int code);
    void GetIncidentByID(String result, int code);
    void GetIncidentQueryResult(String result, int code);
}
