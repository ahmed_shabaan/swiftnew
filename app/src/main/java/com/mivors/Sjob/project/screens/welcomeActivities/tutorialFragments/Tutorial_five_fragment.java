package com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.screens.homeActivity.home;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tutorial_five_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tutorial_five_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tutorial_five_fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView start , startar;

    private OnFragmentInteractionListener mListener;

    public Tutorial_five_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tutorial_five_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Tutorial_five_fragment newInstance(String param1, String param2) {
        Tutorial_five_fragment fragment = new Tutorial_five_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View  rootView = inflater.inflate(R.layout.tutorial_five_fragment, container, false);
        start = (TextView) rootView.findViewById(R.id.textView7);
        startar = (TextView) rootView.findViewById(R.id.textView8);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InHome = new Intent(Tutorial_five_fragment.this.getActivity(), home.class);
                Tutorial_five_fragment.this.getActivity().startActivity(InHome);
                Tutorial_five_fragment.this.getActivity().finish();
            }
        });
        startar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InHome = new Intent(Tutorial_five_fragment.this.getActivity(), home.class);
                Tutorial_five_fragment.this.getActivity().startActivity(InHome);
                Tutorial_five_fragment.this.getActivity().finish();
            }
        });



        String lang =  getResources().getConfiguration().locale.getDisplayLanguage();
        if (lang.equals("English")) {
            System.out.println("enter here667");
            //  parentView.setBackgroundResource(R.mipmap.about);

            start.setVisibility(View.INVISIBLE);
            startar.setVisibility(View.VISIBLE);

        }
        else if (lang.equals("العربية")) {
            System.out.println("enter here668");

            //  parentView.setBackgroundResource(R.mipmap.about_ar);
            start.setVisibility(View.VISIBLE);
            startar.setVisibility(View.INVISIBLE);
        }
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((lastFragmentLoaded)this.getActivity()).getValueFromFragmentFive(false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
