package com.mivors.Sjob.project.screens.showServicesRequestsFragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseFragment;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;
import com.mivors.Sjob.project.screens.filterFragment.FilterFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShowServicesRequestsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShowServicesRequestsFragment extends BaseFragment implements ServiceRequestView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQ_CODE = 1212;
    String from,to,category,status;
    int contactID = 0;

    ArrayList<ServiceRequest> serviceRequestArrayList;

    ServiceRequestAdapter serviceRequestAdapter;

    @Inject
    ServicesRequestsPresenter servicesRequestsPresenter;

    @Bind(R.id.list)
    RecyclerView list;


    public ShowServicesRequestsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShowServicesRequestsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowServicesRequestsFragment newInstance(String param1, String param2) {
        ShowServicesRequestsFragment fragment = new ShowServicesRequestsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceRequestArrayList = new ArrayList<>();
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_show_services_requests, container, false);
        ButterKnife.bind(this,view);
        ((DaggerApplication)getActivity().getApplication()).getAppComponent().inject(this);
        contactID  =  prefManager.getUserID();
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        serviceRequestAdapter = new ServiceRequestAdapter(serviceRequestArrayList,getActivity());
        list.setAdapter(serviceRequestAdapter);
        servicesRequestsPresenter.setView(this);
        servicesRequestsPresenter.getServiceRequests(contactID);

        contactID  =  prefManager.getUserID();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!=null){
            actionBar.setTitle(R.string.mySRs);

        }

        return  view;
    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideLoader() {
        hideProgress();
    }

    @Override
    public void showServiceRequests(ArrayList<ServiceRequest> serviceRequests) {
        serviceRequestArrayList.clear();
        serviceRequestArrayList.addAll(serviceRequests);
        serviceRequestAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestWaring(String s) {
        getWaring(s,1);
    }

    @Override
    public void updateList(ArrayList<ServiceRequest> temp) {
        serviceRequestArrayList.clear();
        serviceRequestArrayList.addAll(temp);
        serviceRequestAdapter.notifyDataSetChanged();
    }

    MenuItem clear =  null;
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter, menu);
        clear  = menu.findItem(R.id.clear);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()  == R.id.filter){
            FilterFragment filterSrFragment = new FilterFragment();
            filterSrFragment.setTargetFragment(this, REQ_CODE);
            filterSrFragment.show(getActivity().getSupportFragmentManager(), "dialog");
            clear.setVisible(true);
        }else if(item.getItemId() == R.id.clear){
            clear.setVisible(false);
            servicesRequestsPresenter.restList();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Utility.UPDATE == 11) {
            servicesRequestsPresenter.getServiceRequests(contactID);
            serviceRequestArrayList.clear();
//            serviceRequestArrayList.addAll(temp);
            serviceRequestAdapter.notifyDataSetChanged();
            list.setAdapter(serviceRequestAdapter);

            Utility.UPDATE = 0;
        }    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_CODE) {
                from   = data.getExtras().getString("from");
                to   = data.getExtras().getString("to");
                category   = data.getExtras().getString("category");
                status   = data.getExtras().getString("status");
                servicesRequestsPresenter.apply_filter(from,to,category,status);
            }
        }

        if(requestCode==Tokens.UPDATE_SR && resultCode  == Tokens.UPDATE_SR){
            servicesRequestsPresenter.getServiceRequests(contactID);
        }
        if(requestCode==Tokens.UPDATE_SR ){
            servicesRequestsPresenter.getServiceRequests(contactID);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
