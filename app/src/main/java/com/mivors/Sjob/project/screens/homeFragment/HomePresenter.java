package com.mivors.Sjob.project.screens.homeFragment;

/**
 * Created by Ahmed shaban on 7/31/2017.
 */

public interface HomePresenter {
    void setView(HomeView view);

    void RequestAirConditioning();
    void RequestPlumbing();
    void RequestElectrical();

    void RequestSatelite();

    void RequestMobile();

    void RequestCleaning();
}
