package com.mivors.Sjob.project.screens.registerActivity;

/**
 * Created by ahmed radwan on 7/31/2017.
 */

public interface RegisterView {
    void showLoader();
    void hideLoader();
    void edEmail();
    void edPassword();
    void edRePassword();
    void btnRegister();
    void requestWaring(String string);
    void checkData();
}
