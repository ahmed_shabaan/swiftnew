package com.mivors.Sjob.project.screens.forgetActivity.restPassword;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.DaggerApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Callback;
import retrofit2.Response;

public class RestPass_Verify extends BaseActivity implements RestPasswordView {

    String Email ;
    Button confirmPasswordBtn ,VerifyBtn  ;
    EditText VerficationCodeET,  PassWord , Confirmassword;
    @Inject
    RestPasswordPresenter presenter;
    @Bind(R.id.change_password_container)
    LinearLayout change_password_container;
    @Bind(R.id.code_container)
    LinearLayout code_container;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restpass_verify);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.forget_password_title_activity);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        ((DaggerApplication)getApplication()).getAppComponent().inject(this);
        presenter.setView(this);
        confirmPasswordBtn = (Button) findViewById(R.id.confirmPassbtn);
        VerficationCodeET = (EditText) findViewById(R.id.verficationCodeET);
        PassWord   = (EditText) findViewById(R.id.passET);
        Confirmassword   = (EditText) findViewById(R.id.confirmpassET);
        VerifyBtn = (Button) findViewById(R.id.verfiybtn);

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            Email = extras.getString("Email");
        }

        VerifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.sendCode(Email);
            }
        });
        confirmPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               presenter.updatedPassword(PassWord.getText().toString(), Confirmassword.getText().toString());
            }
        });

    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideLoader() {
        hideProgress();
    }

    @Override
    public void enableViews() {
        code_container.setVisibility(View.GONE);
        change_password_container.setVisibility(View.VISIBLE);
    }

    @Override
    public void requestWaring(int resourceID) {
       // Toast.makeText(this, getString(resourceID), Toast.LENGTH_LONG).show();
        getWaring(getString(resourceID),0);
    }

    @Override
    public String getInsertedCode() {
        return VerficationCodeET.getText().toString();
    }

    @Override
    public void getSuccess(int resourceID) {
        Toast.makeText(this, getString(R.string.password_updated_successfully), Toast.LENGTH_LONG).show();
        setResult(1);
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



}
