package com.mivors.Sjob.project.helpers;

/**
 * Created by ahmed on 17/09/17.
 */


public enum ContactPaymentMethods {
    Cash(1), Mada(4);
    private final int value;

    private ContactPaymentMethods(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

