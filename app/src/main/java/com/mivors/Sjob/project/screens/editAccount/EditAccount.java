package com.mivors.Sjob.project.screens.editAccount;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.baseClasses.BaseFragment;
import com.mivors.Sjob.project.callBacks.LocationChangeResult;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.homeActivity.home;
import com.mivors.Sjob.project.screens.mapFragment.MainMapFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

public class EditAccount extends BaseFragment implements EditAccountView, LocationChangeResult {

    @Inject
    EditAccountPresenter editAccountPresenter;
    @Inject
    PrefManager prefManager;

    MainMapFragment mainMapFragment;
    FragmentManager fragmentManager;
    String location = null;

    @Bind(R.id.edPhone)
    EditText edPhone;
    @Bind(R.id.edLastName)
    EditText edLastName;
    @Bind(R.id.edFirstName)
    EditText edFirstName;
    @Bind(R.id.edPass)
    EditText edPass;
    @Bind(R.id.edRePass)
    EditText edRePass;
    @Bind(R.id.edAddressDetails)
    EditText edAddressDetails;
    @Bind(R.id.edAddressName)
    EditText edAddressName;
    @Bind(R.id.btnEditAccount)
    Button btnEditAccount;
    @Bind(R.id.spEdit)
    MaterialSpinner spEdit;

    String title;
    int titleId;
    ArrayAdapter<String> dataAdapter;
    private String lat,lng;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_edit_account, container, false);
        ButterKnife.bind(this, view);
        ((DaggerApplication) getActivity().getApplication()).getAppComponent().inject(this);
        editAccountPresenter.setView(this);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.edit_account);

        }
        mainMapFragment = new MainMapFragment();
        fragmentManager = getChildFragmentManager();
//        if(!mainMapFragment.isAdded()) {
            fragmentManager.beginTransaction().replace(R.id.map_frame_layout, mainMapFragment, "mainMapFragment").commit();
//        }
       int titleId = prefManager.getTitleId();
        Log.d("ppp",""+ prefManager.getTitleId());
      int titleSelection =  Utility.integerTitleName(getActivity(),prefManager.getTitle());
        spEdit.setSelection(titleSelection);
        Bundle b = new Bundle();
        b.putString("lat",prefManager.getLat());
        b.putString("lng",prefManager.getLng());
        mainMapFragment.setArguments(b);
        return view;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_account);
//
//
//      }

    @Override
    public void showLoader() {
        showProgress();

    }

    @Override
    public void hideLoader() {

        hideProgress();
    }

    @Override
    public void btnEditAccount() {

        btnEditAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("".equals(edPhone.getText().toString())) {
                    getWaring(getResources().getString(R.string.mobMsgNull), 0);
                } else if ("".equals(edFirstName.getText().toString())) {
                    getWaring(getResources().getString(R.string.EmptyUsrNam), 0);

                } else if ("".equals(edLastName.getText().toString())) {
                    getWaring(getResources().getString(R.string.lstNmeMsgNull), 0);

                } else if ("".equals(edAddressName.getText().toString())) {
                    getWaring(getResources().getString(R.string.choose_map), 0);

                }  else if (!Utility.phoneValidate(edPhone.getText().toString())) {
                    getWaring(getResources().getString(R.string.check_phone), 0);

                } else if ("".equals(edPass.getText().toString())) {
                    getWaring(getResources().getString(R.string.empty_pass), 0);

                } else if ("".equals(edRePass.getText().toString())) {
                    getWaring(getResources().getString(R.string.empty_confirm_pass), 0);

                } else if (!Utility.passValidate(edPass.getText().toString())) {
                    getWaring(getResources().getString(R.string.check_pass), 0);

                } else if (!Utility.passValidate(edRePass.getText().toString())) {
                    getWaring(getResources().getString(R.string.check_re_pass), 0);

                } else if (!Utility.passValidate(edPass.getText().toString(), edRePass.getText().toString())) {
                    getWaring(getResources().getString(R.string.confirm_re_pass), 0);

                }else if ("Title".equalsIgnoreCase(title)||"اللقب".equalsIgnoreCase(title)||getResources().getString(R.string.select_title).equals(title)|"".equals(title)) {
                    getWaring(getResources().getString(R.string.choose_title), 0);

                }

                else if(!Utility.checkCharacterPhone(edPhone.getText().toString())
                        ) {
                    getWaring(getResources().getString(R.string.phone_number_invalid), 0);

                }

                else {
                    User user = new User(Utility.solveCharacter(edFirstName.getText().toString()),
                            Utility.solveCharacter(edLastName.getText().toString()),
                            Utility.solveCharacter(edPass.getText().toString()),
                            edPhone.getText().toString(), title, titleId,
                            Utility.solveCharacter(edAddressName.getText().toString()),
                            Utility.solveCharacter(edAddressDetails.getText().toString()),lat,lng);


                    editAccountPresenter.inputData(user);
                }
            }
        });
    }

    @Override
    public void spinnerView() {
        final String [] strings = getResources().getStringArray(R.array.titles);


        dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, strings);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEdit.setAdapter(dataAdapter);
        spEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    title = strings[position];
                    titleId = position + 1;
                 }catch (Exception e){
                    title = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                title = "";
            }
        });
     }

    @Override
    public void requestData(String string) {
        getSuccess(getResources().getString(R.string.success_edit), Tokens.EDIT_ACCOUNT_SUCCESS);


    }

    @Override
    public void edAddressTitle() {
        if(prefManager.getAddressTitle()!=null) {
            edAddressName.setText(prefManager.getAddressTitle());
        }
        edAddressName.setEnabled(false);
    }

    @Override
    public void edAddressDetails() {
        if(prefManager.getAddressDetails()!=null&&!prefManager.getAddressDetails().equals("")&&!prefManager.getAddressDetails().equalsIgnoreCase("null")) {
            edAddressDetails.setText(prefManager.getAddressDetails());
        }

    }

    @Override
    public void edPass() {
        edPass.setText(prefManager.getPassword());

    }

    @Override
    public void edRePass() {
        edRePass.setText(prefManager.getPassword());

    }

    @Override
    public void edFirstName() {
        if(prefManager.getFirstName()!=null)
            edFirstName.setText(prefManager.getFirstName());
    }

    @Override
    public void edLastName() {
        if(prefManager.getLastName()!=null)
        edLastName.setText(prefManager.getLastName());

    }

    @Override
    public void edPhone() {
        if(prefManager.getPhone()!=null)
            edPhone.setText(prefManager.getPhone());
    }

    @Override
    public void requestWaring(String string) {
        getWaring(string, 1);

    }

    @Override
    public void getAddressLocation(String s,String lat,String lng) {
        try {
            if (s != null) {
                if (!s.isEmpty()) {
                    edAddressName.setText(s);
                }
            }
        }catch (Exception e){

        }
        this.lat = lat;
        this.lng = lng;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mainMapFragment.setMyHomeLocation(true);
        }
    }


}
