package com.mivors.Sjob.project.screens.forgetActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.screens.forgetActivity.restPassword.RestPass_Verify;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForgetPassword extends BaseActivity implements ForgetPassView {
    @Inject
    ForgetPassPresenter forgetPassPresenter;

    @Bind(R.id.btnResetPass)
    Button btnResetPass;

    @Bind(R.id.edEmail)
    EditText edEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);
        forgetPassPresenter.setView(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.forget_password_title_activity);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);   }
    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideLoader() {
        hideProgress();
    }

    @Override
    public void edEmail() {

    }

    @Override
    public void btnForgetPass() {

        btnResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.emailValidates(edEmail.getText().toString())) {
                    forgetPassPresenter.inputData(edEmail.getText().toString());
                }else{
                    getWaring(getResources().getString(R.string.error_invalid_email),0);

                }
            }
        });

    }

    @Override
    public void requestTitle(String s) {
       /* if(s.equals("1")){
            getSuccess(s,4);
        }else{
            getWaring(s,0);

        }*/

        if(s.equalsIgnoreCase(getResources().getString(R.string.sms_send_success))){
            Toast.makeText(this, R.string.code_sent, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, RestPass_Verify.class);
            i.putExtra("Email", edEmail.getText().toString());
            startActivityForResult(i,1);
        }else{
            getWaring(s,0);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode==1){
            finish();
        }
    }
}
