package com.mivors.Sjob.project.screens.EditServiceRequest;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public interface UpdateServiceRequestView {
    void submitServiceRequest();
    void updateDate(String s);
    void updateTime(String s);
    void requestWarring(String s);
    void showSuccess(String s);
    void hidLoader();
    void showLoader();
    void hideVisitSpinner();
    void showVisitSpinner();
    String  getDate();

    String getTime();
}
