package com.mivors.Sjob.project.model.serviceRequest;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ahmed shaban on 8/2/2017.
 */

public class ServiceRequest implements Parcelable {

    String id ;
    int severityLevel;
    int paymentMethod;
    String title ;
    String description;
    int category;
    String GpsLocation;
    String street;
    String Date;
    String houseDescription;
    int IncidentStatues;
    String technicianName;
    String technicianRate;
    String lat;
    String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getActualData() {
        return actualData;
    }

    public void setActualData(String actualData) {
        this.actualData = actualData;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    String actualData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSeverityLevel() {
        return severityLevel;
    }

    public void setSeverityLevel(int severityLevel) {
        this.severityLevel = severityLevel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getGpsLocation() {
        return GpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        GpsLocation = gpsLocation;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getHouseDescription() {
        return houseDescription;
    }

    public void setHouseDescription(String houseDescription) {
        this.houseDescription = houseDescription;
    }

    public int getIncidentStatues() {
        return IncidentStatues;
    }

    public void setIncidentStatues(int incidentStatues) {
        IncidentStatues = incidentStatues;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public String getTechnicianRate() {
        return technicianRate;
    }

    public void setTechnicianRate(String technicianRate) {
        this.technicianRate = technicianRate;
    }

    public String getServiceRate() {
        return serviceRate;
    }

    public void setServiceRate(String serviceRate) {
        this.serviceRate = serviceRate;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public String getComplainTitle() {
        return complainTitle;
    }

    public void setComplainTitle(String complainTitle) {
        this.complainTitle = complainTitle;
    }

    public String getCustomerFeedback() {
        return customerFeedback;
    }

    public void setCustomerFeedback(String customerFeedback) {
        this.customerFeedback = customerFeedback;
    }

    public String getComplainStartTime() {
        return complainStartTime;
    }

    public void setComplainStartTime(String complainStartTime) {
        this.complainStartTime = complainStartTime;
    }

    public String getComplainClosedTime() {
        return complainClosedTime;
    }

    public void setComplainClosedTime(String complainClosedTime) {
        this.complainClosedTime = complainClosedTime;
    }

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public static Creator<ServiceRequest> getCREATOR() {
        return CREATOR;
    }

    String serviceRate;
    String ReferenceNumber;
    String complainTitle;
    String customerFeedback;
    String complainStartTime;
    String complainClosedTime;
    String complaintStatus;

    public ServiceRequest(){}

    protected ServiceRequest(Parcel in) {
        id = in.readString();
        severityLevel = in.readInt();
        title = in.readString();
        description = in.readString();
        category = in.readInt();
        GpsLocation = in.readString();
        street = in.readString();
        Date = in.readString();
        houseDescription = in.readString();
        IncidentStatues = in.readInt();
        technicianName = in.readString();
        technicianRate = in.readString();
        serviceRate = in.readString();
        ReferenceNumber = in.readString();
        complainTitle = in.readString();
        customerFeedback = in.readString();
        complainStartTime = in.readString();
        complainClosedTime = in.readString();
        complaintStatus = in.readString();
        lat = in.readString();
        lng = in.readString();
        paymentMethod = in.readInt();
    }

    public static final Creator<ServiceRequest> CREATOR = new Creator<ServiceRequest>() {
        @Override
        public ServiceRequest createFromParcel(Parcel in) {
            return new ServiceRequest(in);
        }

        @Override
        public ServiceRequest[] newArray(int size) {
            return new ServiceRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(severityLevel);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(category);
        dest.writeString(GpsLocation);
        dest.writeString(street);
        dest.writeString(Date);
        dest.writeString(houseDescription);
        dest.writeInt(IncidentStatues);
        dest.writeString(technicianName);
        dest.writeString(technicianRate);
        dest.writeString(serviceRate);
        dest.writeString(ReferenceNumber);
        dest.writeString(complainTitle);
        dest.writeString(customerFeedback);
        dest.writeString(complainStartTime);
        dest.writeString(complainClosedTime);
        dest.writeString(complaintStatus);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeInt(paymentMethod);
    }
}
