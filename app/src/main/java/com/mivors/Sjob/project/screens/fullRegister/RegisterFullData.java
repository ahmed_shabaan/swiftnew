package com.mivors.Sjob.project.screens.fullRegister;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.callBacks.LocationChangeResult;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.editAccount.EditAccount;
import com.mivors.Sjob.project.screens.homeActivity.home;
import com.mivors.Sjob.project.screens.mapFragment.MainMapFragment;
import com.mivors.Sjob.project.screens.registerActivity.Register;
import com.mivors.Sjob.project.screens.registerActivity.RegisterPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

public class RegisterFullData extends BaseActivity implements FullRegisterView, LocationChangeResult {
    @Inject
    FullRegisterPresenter registerPresenter;

    @Bind(R.id.edAddressName)
    EditText edAddressName;

    @Bind(R.id.edAddressDetails)
    EditText edAddressDetails;

    @Bind(R.id.edFirstName)
    EditText edFirstName;

    @Bind(R.id.edLastName)
    EditText edLastName;

    @Bind(R.id.edPhone)
    EditText edPhone;

    @Bind(R.id.btnCreateAccount)
    Button btnCreateAccount;

    @Bind(R.id.spRegister)
    MaterialSpinner spRegister;

    private String title, email, password;
    MainMapFragment mainMapFragment;
    FragmentManager fragmentManager;
    String location = null;
    int titleId;
    private String lat,lng;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_full_data);
        ButterKnife.bind(this);
        title = getResources().getString(R.string.select_title);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);
        Intent intent = getIntent();
        email = intent.getExtras().getString("email");
        password = intent.getExtras().getString("password");
        mainMapFragment = new MainMapFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.map_frame_layout, mainMapFragment, "mainMapFragment").commit();
         registerPresenter.setView(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.register);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideLoader() {
        hideProgress();
    }

    @Override
    public void btnCreateAccount() {
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if("".equals(edPhone.getText().toString()) ) {
                    getWaring(getResources().getString(R.string.mobMsgNull), 0);
                }else   if("".equals(edFirstName.getText().toString()) ) {
                    getWaring(getResources().getString(R.string.EmptyUsrNam), 0);

                }else if("".equals(edLastName.getText().toString()) ) {
                    getWaring(getResources().getString(R.string.lstNmeMsgNull), 0);

                }else  if("".equals(edAddressName.getText().toString()) ) {
                    getWaring(getResources().getString(R.string.choose_map), 0);

                }
                else if (getResources().getString(R.string.select_title).equalsIgnoreCase(title)) {
                    getWaring(getResources().getString(R.string.choose_title), 0);

                }else if ("Title".equalsIgnoreCase(title)||"اللقب".equalsIgnoreCase(title)||getResources().getString(R.string.select_title).equals(title)||"".equals(title)) {
                    getWaring(getResources().getString(R.string.choose_title), 0);

                } else if(!Utility.phoneValidate(edPhone.getText().toString()) ) {
                    getWaring(getResources().getString(R.string.check_phone), 0);

                }else if(!Utility.checkCharacterPhone(edPhone.getText().toString())) {
                    getWaring(getResources().getString(R.string.phone_number_invalid), 0);

                }

                else {
                    User user = new User(Utility.solveCharacter(edFirstName.getText().toString()),
                            Utility.solveCharacter(edLastName.getText().toString()), email, password,
                            Utility.solveCharacter(edPhone.getText().toString()),
                            title,titleId, Utility.solveCharacter(edAddressName.getText().toString())
                            , Utility.solveCharacter(edAddressDetails.getText().toString()),lat,lng);
                    registerPresenter.inputData(user);

                }

            }
        });
    }

    @Override
    public void spinnerView() {

        final String [] strings = getResources().getStringArray(R.array.titles);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, strings);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRegister.setAdapter(dataAdapter);
        spRegister.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    title = strings[position];
                    titleId = position + 1;
                }catch (Exception e){
                    title = "";

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                title = "";
            }
        });
    }

    @Override
    public void requestData(String string) {
        if (!string.equals("")) {
            //getSuccess(getResources().getString(R.string.success_register), Tokens.SUCCESS_REGISTER);
            prefManager.setLogin();
             setResult(Tokens.SUCCESS_REGISTER);

             Toast.makeText(this,getResources().getString(R.string.success_register), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void requestWaring(String string) {
        getWaring(string,1);

    }



    @Override
    public void getAddressLocation(String s,String lat,String lng) {

        edAddressName.setText(s);
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mainMapFragment.setMyHomeLocation(true);
        }
    }

}
