package com.mivors.Sjob.project.screens.editAccount;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.RightNow.RNObject;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassPresenter;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassView;

import org.json.JSONException;

import javax.inject.Inject;

/**
 * Created by ahmed radwan on 8/1/2017.
 */

public class EditAccountPresenterImpl implements EditAccountPresenter,ContactCallBacks {


    EditAccountView view;
    @Inject
    ApiManager apiManager;

    @Inject
    PrefManager prefManager;

    @Inject
    Context context;
    public EditAccountPresenterImpl(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
    }
    @Override
    public void setView(EditAccountView view) {
        this.view = view;
        view.btnEditAccount();
        view.spinnerView();
        view.edFirstName();
        view.edLastName();
        view.edPass();
        view.edRePass();
        view.edAddressTitle();
        view.edAddressDetails();
        view.edPhone();
     }

    @Override
    public void inputData(User user) {

            view.showLoader();
            apiManager.RegisterContactCallback(this);
            RNObject rnObject = new RNObject(GeneralTypesEnum.contacts.name());
            rnObject.setId(prefManager.getUserID());
           prefManager.set_data(user);
        prefManager.setTitleId(user.getTitleId());

            String data = null;
            try {
                data = Utility.jsonUpdate(user);
                apiManager.Update(rnObject,data);

            } catch (JSONException e) {
                e.printStackTrace();
             }

    }

    @Override
    public void CreateContact(String result, int code) {

    }

    @Override
    public void UpdateContact(String result, int code) {
        view.hideLoader();
        String re = result;
        if(code==200||code==201) {

            view.requestData(result);
        }else if(code==0){
            view.requestWaring(context.getString(R.string.internet_error_connection));
         Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
                    ,"EditAccount");

        }else{
            view.requestWaring(context.getString(R.string.sms_send_error));
              Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"EditAccount");

        }

    }

    @Override
    public void GetContactByID(String result, int code) {

    }

    @Override
    public void GetContactQueryResult(String result, int code) {

    }


}
