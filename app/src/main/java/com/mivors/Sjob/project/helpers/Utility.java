package com.mivors.Sjob.project.helpers;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.mivors.Sjob.R;
import com.mivors.Sjob.project.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

/**
 * Created by ahmed radwan on 7/31/2017.
 */

public class Utility {

     PrefManager prefManager;
    public static final String HISTORY_INCIDENT = "Select Contacts.ID,Contacts.CustomFields.c.password,Contacts.CustomFields.c.phone_number,Contacts.CustomFields.TOA.near_by,Contacts.CustomFields.TOA.street,name.first,name.last,Contacts.title,Contacts.CustomFields.TOA.coordinate_x,Contacts.CustomFields.TOA.coordinate_y FROM Contacts where Contacts.Emails.EmailList.Address = ";
    public static final String FORGET_PASS = "https://swiftcare.custhelp.com/cc/ContactController/resetPassViaSMS";
    // public static final String PASS = "{customFields":{"c":{"password":
    public static final String LOGIN_CHECK = "Select Contacts.ID,Contacts.CustomFields.c.password,Contacts.Emails.EmailList.Address FROM Contacts where Contacts.Emails.EmailList.Address = ";

    public static int LOGIN_ONCE = 0;
    public static int UPDATE = 0;

    public Utility(PrefManager prefManager) {
        this.prefManager = prefManager;
    }

    public static boolean emailValidates(String email) {
        return email != null &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean containData(String password) {
        return password != null && password.length() >= 5;
    }

    public static String loginUrl(String email) {
        return HISTORY_INCIDENT + "\'" + email + "\'";
    }
    public static String checkEmail(String email) {
        return LOGIN_CHECK + "\'" + email + "\'";
    }
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean phoneValidate(String phone) {
        return phone.length() == 9;
    }
    public static boolean passValidate(String pass) {
        return pass.length() >= 5;
    }
    public static boolean passValidate(String pass, String rePass) {
        return  pass.equals(rePass);
    }

    public   String jsonPass(String json) throws JSONException {
        String pass = "";
        JSONObject jsonObject = new JSONObject(json);
        JSONArray jsonArray = jsonObject.getJSONArray("items");
        JSONObject jsonObjects = jsonArray.getJSONObject(0);

        JSONArray jsonArrays = jsonObjects.getJSONArray("rows");
        for (int i = 0; i < jsonArrays.length(); i++) {

            JSONArray jj = jsonArrays.getJSONArray(i);
            int id = jj.getInt(0);
            pass = jj.getString(1);
            String phone = jj.getString(2);
            String houseDetails = jj.getString(3);
            String street = jj.getString(4);
            String firstName = jj.getString(5);
            String lastName = jj.getString(6);
            String title = jj.getString(7);

            User user = new User();
            user.setPhone(phone);
            user.setAddressDetails(houseDetails);
            user.setAddressTitle(street);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setTitle(title);
            user.setId(id);
            user.setLng(jj.getString(8));
            user.setLat(jj.getString(9));

            prefManager.setId(id);
            prefManager.set_data(user);

        }

        return pass;
    }
    public   String[] jsonPassRegister(String json) throws JSONException {
        String pass = "";
        String email = "";
        JSONObject jsonObject = new JSONObject(json);
        JSONArray jsonArray = jsonObject.getJSONArray("items");
        JSONObject jsonObjects = jsonArray.getJSONObject(0);

        JSONArray jsonArrays = jsonObjects.getJSONArray("rows");
        for (int i = 0; i < jsonArrays.length(); i++) {

            JSONArray jj = jsonArrays.getJSONArray(i);
            int   id = jj.getInt(0);
            pass = jj.getString(1);
            email = jj.getString(2);




        }

        return new String[]{pass,email};
    }
    public  String jsonRegister(String json) throws JSONException {
        String id = "";
         JSONObject jsonObject = new JSONObject(json);
        id = jsonObject.getInt("id") + "";
        prefManager.setId(jsonObject.getInt("id"));

        return id;
    }

    public static String jsonCreate(User user) throws JSONException {
        JSONObject json = new JSONObject();

        JSONObject jsonObjectCustomField = new JSONObject();
        JSONObject jsonObjectC = new JSONObject();

        jsonObjectCustomField.put("c", jsonObjectC);
        jsonObjectC.put("password", user.getPassword());
        jsonObjectC.put("phone_number", user.getPhone());
        jsonObjectC.put("gpslocation", "android");
         if(!user.getAddressDetails().isEmpty()) {
            jsonObjectC.put("house_description",user.getAddressDetails());
        }else {
            jsonObjectC.put("house_description","\"null\"");
        }

        JSONObject jsonObjectTOA = new JSONObject();

        JSONObject jsonObjectCity = new JSONObject();
        jsonObjectCity.put("id",1);
        jsonObjectTOA.put("city",  jsonObjectCity);
        JSONObject jsonObjectCountry = new JSONObject();
        jsonObjectCountry.put("id",1);
        jsonObjectTOA.put("country", jsonObjectCountry);
        jsonObjectTOA.put("street", user.getAddressTitle());
        jsonObjectTOA.put("coordinate_x", user.getLng());
        jsonObjectTOA.put("coordinate_y", user.getLat());
        jsonObjectTOA.put("google_location",getGoogleLocation(user.getLat(),user.getLng()));
        if(!user.getAddressDetails().isEmpty()) {
            jsonObjectTOA.put("near_by", user.getAddressDetails());
        }else {
            jsonObjectTOA.put("near_by", "\"null\"");
        }
        jsonObjectCustomField.put("TOA", jsonObjectTOA);

        json.put("customFields", jsonObjectCustomField);
        json.put("title", user.getTitle());

        JSONArray jsonArrayEmail = new JSONArray();

        JSONObject jsonObjectAddress = new JSONObject();
        jsonObjectAddress.put("address", user.getEmail());

        JSONObject jsonObjectAddressTypeId = new JSONObject();
        jsonObjectAddressTypeId.put("id", 0);
        jsonObjectAddress.put("addressType", jsonObjectAddressTypeId);
        jsonArrayEmail.put(jsonObjectAddress);

        json.put("emails", jsonArrayEmail);

        JSONObject jsonType = new JSONObject();
        jsonType.put("id",1);
         json.put("contactType", jsonType);

        JSONObject jsonObjectName = new JSONObject();
        jsonObjectName.put("first", user.getFirstName());
        jsonObjectName.put("last", user.getLastName());
        json.put("name", jsonObjectName);
         return json.toString();
    }

    public static String jsonUpdate(User user) throws JSONException {
        JSONObject json = new JSONObject();

        JSONObject jsonObjectCustomField = new JSONObject();
        JSONObject jsonObjectC = new JSONObject();
        jsonObjectCustomField.put("c", jsonObjectC);
        jsonObjectC.put("password", user.getPassword());
        jsonObjectC.put("phone_number", user.getPhone());
        jsonObjectC.put("gpslocation", "android");
        if(!user.getAddressDetails().isEmpty()) {
            jsonObjectC.put("house_description",user.getAddressDetails());
        }else {
            jsonObjectC.put("house_description","\"null\"");

        }
        JSONObject jsonObjectTOA = new JSONObject();

        jsonObjectTOA.put("street", user.getAddressTitle());
        jsonObjectTOA.put("coordinate_x", user.getLng());
        jsonObjectTOA.put("coordinate_y", user.getLat());
        jsonObjectTOA.put("google_location",getGoogleLocation(user.getLat(),user.getLng()));
        if(!user.getAddressDetails().isEmpty()){
            jsonObjectTOA.put("near_by", user.getAddressDetails());

        }else {
            jsonObjectTOA.put("near_by","\"null\"");

        }
        jsonObjectCustomField.put("TOA", jsonObjectTOA);



//        JSONObject jsonAddress = new JSONObject();
//        json.put("address", jsonAddress);
//
//        jsonAddress.put("street", user.getAddressTitle());

        json.put("customFields", jsonObjectCustomField);
        json.put("title", user.getTitle());

        JSONObject jsonObjectName = new JSONObject();
        jsonObjectName.put("first", user.getFirstName());
        jsonObjectName.put("last", user.getLastName());
        json.put("name", jsonObjectName);
         return json.toString();
    }

    public static int integerTitleName(Context context,String provider){
        if("MR".equalsIgnoreCase(provider)||
               "السيد".equalsIgnoreCase(provider)){
            return 1;
        }else if("MS".equalsIgnoreCase(provider)||
                "السيدة".equalsIgnoreCase(provider)) {
            return 2;

        }else if("MRS".equalsIgnoreCase(provider)||
                "الأستاذ".equalsIgnoreCase(provider)) {
            return 3;

        }else if("MIS".equalsIgnoreCase(provider)||
        "الأستاذة".equalsIgnoreCase(provider)) {
            return 4;

        }else {
            return 0;
        }
        }

        public static boolean checkCharacterPhone(String s) {
            return !s.contains(".") && !s.contains(",") && !s.contains("'") && !s.contains("#") && !s.contains("*");
        }

    public static boolean checkCharacter(String s) {
        return  !s.contains(",") && !s.contains("'") && !s.contains("\"") ;
    }

    public static String solveCharacter(String s) {
        String ss = "";
            ss = s.replace("\"", " ").replace("\\\\"," ").replace("\n"," ").replace(","," ").replace("'"," ").replace(":"," ").replace("|"," ").replace(";"," ");

        return  ss;
    }

    public static String getGoogleLocation(String lat,String lng) {
        return "http://maps.google.com/maps?saddr="+lat+","+lng+"&daddr="+lat+","+lng+"";

    }

    public static void setCrashlyticsLog(int code ,String result ,String message ,String className){
        try {
            Crashlytics.setString(className + "  server_response ", result);
            Crashlytics.setInt(className + " code error ", code);
            Crashlytics.log(className + "  server_response  " + result + "  \n  code error    " + code);
            Crashlytics.setString(className + "  message ", message);
            Crashlytics.logException(new Exception(className + " :    " + code + "  \n response  " + result));
            Answers.getInstance().onException(new Crash.FatalException(className + "  :    " + code + "  \n response  " + result + "  message " + message));
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }
}
