package com.mivors.Sjob.project.screens;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.RNClasses.Incident.Incident;
import com.mivors.Sjob.RightNow.RNClasses.Incident.IncidentCallBacks;
import com.mivors.Sjob.project.baseClasses.BaseFragment;
import com.mivors.Sjob.project.helpers.DaggerApplication;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements IncidentCallBacks{
    @Inject
    ApiManager ApiManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);


        List<Incident>incidents  = new ArrayList<>();
        Incident s1 = new Incident("incident");
        incidents.add(s1);
        ApiManager.RegisterIncidentCallback(this);
        //Incident incident = new Incident(GeneralTypesEnum.incidents.toString());
        //ApiManager.GetQueryResult("SELECT id FROM Incidents LIMIT 25");

    }

    @Override
    public void CreateIncident(String result, int code) {

    }

    @Override
    public void UpdateIncident(String result, int code) {

    }

    @Override
    public void GetIncidentByID(String result, int code) {

    }

    @Override
    public void GetIncidentQueryResult(String result, int code) {

    }

    /**
     * A simple {@link Fragment} subclass.
     * Use the {@link showServicesRequests#newInstance} factory method to
     * create an instance of this fragment.
     */
    public static class showServicesRequests extends BaseFragment {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private static final String ARG_PARAM1 = "param1";
        private static final String ARG_PARAM2 = "param2";

        // TODO: Rename and change types of parameters
        private String mParam1;
        private String mParam2;


        public showServicesRequests() {
            // Required empty public constructor
        }

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ShowServicesRequestsFragment.
         */
        // TODO: Rename and change types and number of parameters
        public static showServicesRequests newInstance(String param1, String param2) {
            showServicesRequests fragment = new showServicesRequests();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                mParam1 = getArguments().getString(ARG_PARAM1);
                mParam2 = getArguments().getString(ARG_PARAM2);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View  view  = inflater.inflate(R.layout.fragment_show_services_requests, container, false);



            return view;
        }

    }
}
