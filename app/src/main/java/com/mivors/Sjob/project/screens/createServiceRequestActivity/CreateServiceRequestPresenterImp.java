package com.mivors.Sjob.project.screens.createServiceRequestActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Incident.Incident;
import com.mivors.Sjob.RightNow.RNClasses.Incident.IncidentCallBacks;
import com.mivors.Sjob.project.helpers.Categories;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.DateAndTimeOperations;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.SRPaymentMethods;
import com.mivors.Sjob.project.helpers.Severities;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public class CreateServiceRequestPresenterImp implements CreateServiceRequestPresenter, IncidentCallBacks {
    CreateServiceRequestView createServiceRequestView;
    Context context;
    String date, time;

    @Inject
    ApiManager apiManager;

    @Inject
    PrefManager prefManager;

    @Inject
    DateAndTimeOperations dateAndTimeOperations;

    int years, months, days;
    private int hour, minute, hourOfDay;
    static int dayss, monthss;
    int selectedYears,selectedMonth,SelectedDay;

    @Override
    public void setView(CreateServiceRequestView view, Context context) {
        this.createServiceRequestView = view;
        this.context = context;
    }

    public CreateServiceRequestPresenterImp(Context context) {
        ((DaggerApplication) context).getAppComponent().inject(this);
    }

    @Override
    public void RequestSubmitServiceRequest(int paymentIndex, int severityIndex, int visitIndex, String description, String location, String locationDetails, int contact_id, int categoryId,String lat,String lng) {
        String paymentMethod = null, severityLevel, fianlDateTime = null;
        boolean isSuccess = true;
        if (paymentIndex == 0) {
            createServiceRequestView.requestWarring(context.getString(R.string.request_payment_method));
            isSuccess = false;
            return;
        }

        else if (paymentIndex == 3)
        {
            isSuccess = false;
            createServiceRequestView.requestWarring(context.getString(R.string.credit_Card_Not_supported));
            return;
        }
        else {
            paymentMethod = context.getResources().getStringArray(R.array.paymentMethods_array)[paymentIndex - 1];
        }


        if (severityIndex == 0) {
            createServiceRequestView.requestWarring(context.getString(R.string.emptySev));
            isSuccess = false;
            return;
        } else {
            severityLevel = context.getResources().getStringArray(R.array.severity_array)[severityIndex - 1];
        }


        if (visitIndex == 0) {
            createServiceRequestView.requestWarring(context.getString(R.string.emptyPrefVst));
            isSuccess = false;
            return;
        } else if (visitIndex == 1) {
            fianlDateTime = dateAndTimeOperations.setRNDdateFormat(dateAndTimeOperations.getCurrentDate(), dateAndTimeOperations.getCurrentTime());
        } else if (visitIndex == 2) {
            if (date != null && time != null) {
                fianlDateTime = dateAndTimeOperations.setRNDdateFormat(date, dateAndTimeOperations.convertToTwentyFourHours(time));
            } else {
                isSuccess = false;
                createServiceRequestView.requestWarring(context.getString(R.string.dateMsgNull));
                return;
            }
        }

        if (location.isEmpty()) {
            createServiceRequestView.requestWarring(context.getString(R.string.location_not_detected));
            isSuccess = false;
            return;

        }

        if (isSuccess) {
            int severityLevelId = 0;
            if (severityLevel.equals(context.getString(R.string.simple))) {
                severityLevelId = Severities.simple.getValue();
            } else if (severityLevel.equals(context.getString(R.string.moderate))) {
                severityLevelId = Severities.moderate.getValue();
            } else if (severityLevel.equals(context.getString(R.string.complex))) {
                severityLevelId = Severities.complex.getValue();
            }

            String title = null;

            if (Categories.aircondition.getValue() == categoryId) {
                title = context.getString(R.string.airCond);
            } else if (Categories.plumbing.getValue() == categoryId) {
                title = context.getString(R.string.plumb);
            } else if (Categories.electrical.getValue() == categoryId) {
                title = context.getString(R.string.elec);

            }else if(Categories.cleaning.getValue()==categoryId){
                title = context.getString(R.string.cleaning);
            }else if(Categories.mobile.getValue()==categoryId){
                title = context.getString(R.string.mobile);
            }else if(Categories.satellite.getValue()==categoryId){
                title = context.getString(R.string.satellite);
            }


            if(categoryId==Categories.cleaning.getValue() || categoryId==Categories.mobile.getValue()|| categoryId==Categories.satellite.getValue()){
                severityLevelId = Severities.simple.getValue();
            }

            int paymentMethodID=0;

            if(paymentIndex==1)
            {
                paymentMethodID = SRPaymentMethods.Cash.getValue();
            }
            if(paymentIndex==2)
            {
                paymentMethodID =  SRPaymentMethods.Mada.getValue();;
            }

            if(locationDetails.isEmpty()){
                locationDetails = "null";
            }
            if (prefManager.check_login() || Utility.LOGIN_ONCE == 1) {

                String google_location = "http://maps.google.com/maps?saddr="+lat+","+lng+"&daddr="+lat+","+lng+"";
                String request = "{\n" +
                        " \"primaryContact\": {\n" +
                        "   \"id\": " + contact_id + "\n" +
                        " },\n" +
                        " \"subject\": \"" + title + "\",\n" +
                        " \"customFields\": {\n" +
                        "   \"c\": {\n" +
                        "     \"gpslocation\": \"gps\",\n" +
                        "     \"description\": \"" + description + "\",\n" +
                        "     \"date\": \"" + fianlDateTime + "\",\n" +
                        "     \"severity_level\": {\n" +
                        "       \"id\": " + severityLevelId + "\n" +
                        "     },\n" +
                        "     \"c_product\":{\n" +
                        "       \"id\": " + categoryId + "\n" +
                        "     } ,\n" +
                        "     \"sr_payment_method\":{\n" +
                        "       \"id\": " + paymentMethodID + "\n" +
                        "     } ,\n" +
                        "     \"house_description\": \"" + locationDetails + "\"\n" +
                        "   },\n" +
                        "   \"TOA\": {\n" +
                        "     \"country\":{\n" +
                        "       \"id\": 1\n" +
                        "     },\n" +
                        "     \"street\": \"" + location + "\",\n" +
                        "     \"near_by\": \"" + locationDetails + "\",\n" +
                        "     \"coordinate_x\": \""+ lng +"\",\n" +
                        "     \"coordinate_y\": \""+ lat +"\",\n" +
                        "     \"google_location\": \""+ google_location +"\",\n" +
                        "     \"city\": {\n" +
                        "       \"id\": 1\n" +
                        "     }\n" +
                        "   }\n" +
                        " }\n" +
                        "}";


                apiManager.RegisterIncidentCallback(this);
                Incident incident
                        = new Incident(GeneralTypesEnum.incidents.name());
                apiManager.Create(incident, request);
                createServiceRequestView.showLoader();
            } else {
                createServiceRequestView.showLoginNeed();

                //  getWaring(getString(R.string.login_first_navigation), Tokens.LOGIN_NAVIGATION);
            }


        }


    }

    @Override
    public void RequestClock() {
        hour = 0;
        minute = 0;
        TimePickerDialog tpd = new TimePickerDialog(context, R.style.datepicker,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        int hourOfDayas = hourOfDay;
                        String am_pm = "";
                        if (hourOfDay < 12) {
                            am_pm = "AM";
                            if (hourOfDay == 0) {
                                hourOfDay = 12;
                            }
                        } else {
                            am_pm = "PM";
                            hourOfDay = hourOfDay - 12;
                        }
                        NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
                        nf.format(hourOfDay);
                        nf.format(minute);

                        time = nf.format(hourOfDay) + ":" + nf.format(minute) + ":" + context.getResources().getString(R.string.seconds) + " " + am_pm;
                        Calendar datetime = Calendar.getInstance();
                        Calendar c = Calendar.getInstance();
                        Calendar datetimes = Calendar.getInstance();
                        datetimes.set(Calendar.HOUR_OF_DAY, hourOfDayas);
                        datetimes.set(Calendar.MINUTE, minute);
                        if (dayss != 0) {
                            datetimes.set(Calendar.YEAR, years);
                            datetimes.set(Calendar.MONTH, monthss);
                            datetimes.set(Calendar.DAY_OF_MONTH, dayss);
                            datetimes.set(Calendar.HOUR_OF_DAY, hourOfDayas);
                            datetimes.set(Calendar.MINUTE, minute);
                        }
                        int secons = datetime.get(Calendar.SECOND);

                        if (datetimes.getTimeInMillis() >= c.getTimeInMillis()) {

                            createServiceRequestView.updateTime(nf.format(hourOfDay) + ":" + nf.format(minute) + ":" + nf.format(secons) + " " + am_pm);
                        } else

                        {
                            createServiceRequestView.updateTime("future");
                        }
                    }
                }, hour, minute, true);
        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR);
        minute = c.get(Calendar.MINUTE);
        tpd.updateTime(hourOfDay, minute);

        tpd.show();
    }

    @Override
    public void RequestCalender() {
        final Calendar c = Calendar.getInstance();

        years = c.get(Calendar.YEAR);
        months = c.get(Calendar.MONTH);
        days = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(context, R.style.datepicker,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        date = year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth;
                        //    createServiceRequestView.updateDate(date);
//                        Calendar datetime = Calendar.getInstance();
                        Calendar c = Calendar.getInstance();
                        Calendar datetimes = Calendar.getInstance();
                        datetimes.set(Calendar.YEAR, year);
                        datetimes.set(Calendar.MONTH, (monthOfYear));

                        datetimes.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        dayss = dayOfMonth;
                        monthss = monthOfYear;
                        years = year;
                        if (datetimes.getTimeInMillis() >= c.getTimeInMillis()) {
                            createServiceRequestView.updateDate(date);
                        } else {

                            createServiceRequestView.updateDate("future_date");
                        }


                    }
                }, years, months, days);
        dpd.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
        ;    // Set minimum date the current date
        dpd.show();
    }

    @Override
    public void changeVisitStatus(int position) {
        if (position == 0) {
            createServiceRequestView.hideVisitSpinner();
        } else if (position == 1) {
            createServiceRequestView.showVisitSpinner();

        }
    }

    @Override
    public void CreateIncident(String result, int code) {
        createServiceRequestView.hidLoader();

        if (code == 201) {
            createServiceRequestView.showSuccess(context.getString(R.string.create_services_success));

        } else if (code == 408) {
            createServiceRequestView.showSuccess(context.getResources().getString(R.string.sms_send_error));
            Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"CreateServiceRequest");

        }else if (code ==0){
            createServiceRequestView.showSuccess(context.getResources().getString(R.string.internet_error_connection));
            Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
                    ,"CreateServiceRequest");
        }
        else {
            createServiceRequestView.requestWarring(context.getString(R.string.sms_send_error));
             Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"CreateServiceRequest");


        }


    }

    @Override
    public void UpdateIncident(String result, int code) {

    }

    @Override
    public void GetIncidentByID(String result, int code) {

    }

    @Override
    public void GetIncidentQueryResult(String result, int code) {

    }
}
