package com.mivors.Sjob.project.screens.showServicesRequestsFragment;

/**
 * Created by Ahmed shaban on 8/2/2017.
 */

public interface ServicesRequestsPresenter {
    void setView(ServiceRequestView view);
    void getServiceRequests(int contactId);

    void apply_filter(String from, String to, String category, String status);

    void restList();
}
