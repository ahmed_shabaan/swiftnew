package com.mivors.Sjob.project.model.serviceRequest;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmed shaban on 8/2/2017.
 */

public class ServicesRequestParser {

    public static ArrayList<ServiceRequest> ParseServiceRequests(String data) throws JSONException {

        JSONObject jsonObject = new JSONObject(data);
        JSONArray jsonArray = jsonObject.getJSONArray("items");
        JSONObject jsonObjects = jsonArray.getJSONObject(0);

        ArrayList<ServiceRequest> serviceRequests = new ArrayList<>();

        JSONArray jsonArrays = jsonObjects.getJSONArray("rows");
        for (int i = 0; i < jsonArrays.length(); i++) {
            ServiceRequest serviceRequest = new ServiceRequest();
            JSONArray obj = jsonArrays.getJSONArray(i);
            serviceRequest.setCategory(obj.getInt(0));
            serviceRequest.setId(obj.getString(1));
            serviceRequest.setSeverityLevel(obj.getInt(2));
            serviceRequest.setDescription(obj.getString(3));
            serviceRequest.setTitle(obj.getString(4));
            serviceRequest.setGpsLocation(obj.getString(5));
            serviceRequest.setHouseDescription(obj.getString(6));
            serviceRequest.setDate(obj.getString(7));
            serviceRequest.setStreet(obj.getString(9));
            serviceRequest.setIncidentStatues(obj.getInt(10));
            serviceRequest.setTechnicianRate(obj.getString(11));
            serviceRequest.setServiceRate(obj.getString(12));
            serviceRequest.setReferenceNumber(obj.getString(13));
            serviceRequest.setComplainTitle(obj.getString(15));
            serviceRequest.setCustomerFeedback(obj.getString(16));
            serviceRequest.setComplainStartTime(obj.getString(17));
            serviceRequest.setComplainClosedTime(obj.getString(18));
            serviceRequest.setComplaintStatus(obj.getString(19));
            serviceRequest.setLng(obj.getString(21));
            serviceRequest.setLat(obj.getString(22));
            serviceRequest.setPaymentMethod(obj.getInt(23));
            serviceRequests.add(serviceRequest);
        }

        return serviceRequests;
    }


 }
