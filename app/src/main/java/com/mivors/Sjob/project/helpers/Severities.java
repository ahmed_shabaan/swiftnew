package com.mivors.Sjob.project.helpers;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public enum Severities {
    simple(24), moderate(25),complex(26);
    private final int value;

    private Severities(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
