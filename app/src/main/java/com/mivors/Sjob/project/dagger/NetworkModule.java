package com.mivors.Sjob.project.dagger;

import android.content.Context;

import com.mivors.Sjob.RightNow.ApiClient.ApiInterface;
import com.mivors.Sjob.RightNow.ApiClient.ServicesConnection;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

@Module
public class NetworkModule {
    private static final String NAME_BASE_URL = "NAME_BASE_URL";

    @Provides
    @Named(NAME_BASE_URL)
    String provideBaseUrlString() {
        return ServicesConnection.BASE_URL;
    }

    @Provides
    @Singleton
    Converter.Factory provideGsonConverter() {
        return ScalarsConverterFactory.create();
    }



    @Provides
    @Singleton
    Interceptor  providesInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                    request = request.newBuilder()
                            .addHeader("Content-Type", ServicesConnection.CONTENT_TYPE)
                            .addHeader("Authorization", ServicesConnection.getBasicAuthentication())
                            .build();


                return chain.proceed(request);
            }
        };
    }

    @Provides
    @Singleton
    OkHttpClient  provideOkHTTp(Interceptor interceptor){
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }


    @Provides
    @Singleton
    Retrofit provideRetrofit( @Named(NAME_BASE_URL) String baseUrl,Converter.Factory converter) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .build();
    }

    @Provides
    @Singleton
    ApiInterface provideUsdaApi(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

    @Provides
    @Singleton
    ApiManager provideApiManager(Context context) {
        return new ApiManager(context);
    }

}
