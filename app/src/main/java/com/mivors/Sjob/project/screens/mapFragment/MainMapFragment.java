package com.mivors.Sjob.project.screens.mapFragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseFragment;
import com.mivors.Sjob.project.callBacks.LocationChangeResult;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.MapStateListener;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.TouchableMapFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public class MainMapFragment extends BaseFragment implements

        OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, DialogInterface.OnClickListener {


    protected View rootView;
    protected RelativeLayout map;
    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    android.app.FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    PlaceAutocompleteFragment placeAutocompleteFragment;
    private String full_address;
    private Polygon polygon;
    private Marker TempMarker;
    TouchableMapFragment mapFragment;
    ProgressBar progress;
    int mapId = 0;
    double lat = 0, lng = 0;
    View mapView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {
                Bundle bundle = getArguments();
                lat = Double.parseDouble(bundle.getString("lat"));
                lng = Double.parseDouble(bundle.getString("lng"));
            }

        } catch (Exception e) {

        }
    }

    LocationChangeResult locationChangeResult;
    ImageView refresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.main_map_fragment, container, false);
        }

        refresh = (ImageView) rootView.findViewById(R.id.reload);

        mapFragment = (TouchableMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
        fragmentManager = getActivity().getFragmentManager();
        placeAutocompleteFragment = new PlaceAutocompleteFragment();
        fragmentManager.beginTransaction().replace(R.id.place_autocomplete_fragment, placeAutocompleteFragment).commit();
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                String G = place.getAddress().toString();
                Geocoder geocoder = new Geocoder(getActivity());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocationName(G, 3);
                    if (addresses != null && !addresses.equals("")) {
                        search(addresses);
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Status status) {

            }
        });


        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Toast.makeText(getActivity(), "GPS is Enabled in your device", Toast.LENGTH_SHORT).show();
        } else {
            showGPSDisabledAlertToUser();
        }


        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        return rootView;
    }


    public void search(List<Address> addresses) {
        Address address = addresses.get(0);
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
        String addressText = String.format(
                "%s, %s",
                address.getMaxAddressLineIndex() > 0 ? address
                        .getAddressLine(0) : "", address.getCountryName());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(addressText);
        addressText = reverseGeocoding(markerOptions.getPosition().latitude, markerOptions.getPosition().longitude);
        Boolean inside = pointInPolygon(markerOptions.getPosition(), polygon);
        if (inside) {
            mMap.clear();
            markerOptions.position(latLng);
            markerOptions.title(addressText);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            locationChangeResult.getAddressLocation(full_address, markerOptions.getPosition().latitude + "", markerOptions.getPosition().longitude + "");

        } else {
            getWaring(getString(R.string.riyadh_support), 1);
        }

    }

    public String reverseGeocoding(Double lat, Double lng) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Geocoder geo = new Geocoder(getActivity(), new Locale(prefManager.get_lang()));
        List<Address> addresses;
        try {


            addresses = geo.getFromLocation(lat, lng, 1);
            if (addresses.isEmpty()) {
                System.out.println("Waiting for Location");
                builder.setTitle("");
                builder.setMessage(R.string.wtn4gps);
                builder.setCancelable(false);
                builder.setNeutralButton(R.string.okMsg, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

            } else {
                if (addresses.size() > 0) {
                    addresses = geo.getFromLocation(lat, lng, 1);
                    String street, prov, teritory, city, country, countryCode,sublocality,locality;

                    if (addresses.get(0).getThoroughfare() == null) {
                        street = "";
                    } else {
                        street = addresses.get(0).getThoroughfare();
                    }
                    if (addresses.get(0).getSubLocality() == null) {
                        sublocality = "";
                    } else {
                        sublocality = addresses.get(0).getSubLocality();
                    }
                    if (addresses.get(0).getLocality() == null) {
                        locality = "";
                    } else {
                        locality = addresses.get(0).getLocality();
                    }
                    if (addresses.get(0).getAddressLine(3) == null) {
                        teritory = "";
                    } else {
                        teritory = addresses.get(0).getAddressLine(3);
                    }
                    if (addresses.get(0).getCountryName() == null) {
                        country = "";
                    } else {
                        country = addresses.get(0).getCountryName();
                    }
                    if (addresses.get(0).getCountryCode() == null) {
                        countryCode = "";
                    } else {
                        countryCode = addresses.get(0).getCountryCode();
                    }
                    full_address = street + " / " + sublocality + " / " + locality+ " / " + country;
                    LatLng point = new LatLng(lat, lng);
                    Boolean inside = pointInPolygon(point, polygon);
                    return full_address;
                }

            }
        } catch (IOException ie) {

        }
        return "";
    }


    @Override
    public void onStart() {
        googleApiClient.connect();
        super.onStart();
    }


    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getArguments() != null)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 17));
        polygon = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(24.643236, 46.287238),
                        new LatLng(24.733751, 46.322758),
                        new LatLng(24.775707, 46.332225),
                        new LatLng(24.846120, 46.387751),
                        new LatLng(24.869088, 46.475940),
                        new LatLng(24.916493, 46.487372),
                        new LatLng(24.990453, 46.457596),
                        new LatLng(25.023942, 46.429119),
                        new LatLng(25.063175, 46.372719),
                        new LatLng(25.095599, 46.361171),
                        new LatLng(25.123832, 46.351932),
                        new LatLng(25.146309, 46.350777),
                        new LatLng(25.157807, 46.417178),
                        new LatLng(25.144741, 46.635435),
                        new LatLng(25.155194, 46.839256),
                        new LatLng(25.163556, 47.236506),
                        new LatLng(24.932045, 47.331483),
                        new LatLng(24.754226, 47.149596),
                        new LatLng(24.698070, 47.095506),
                        new LatLng(24.452854, 47.208665),
                        new LatLng(24.334277, 47.100456),
                        new LatLng(24.297443, 46.962048),
                        new LatLng(24.375935, 46.777505),
                        new LatLng(24.408285, 46.708913),
                        new LatLng(24.435051, 46.627665),
                        new LatLng(24.454750, 46.576222),
                        new LatLng(24.462183, 46.545601),
                        new LatLng(24.469616, 46.539885),
                        new LatLng(24.469412, 46.533166),
                        new LatLng(24.552005, 46.368009))

                .strokeColor(Color.GREEN)
                .fillColor(Color.TRANSPARENT));


        MapActions();


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        setLocationPosition();


    }

    private void setLocationPosition() {
        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).
                getParent()).findViewById(Integer.parseInt("2"));

        // and next place it, for example, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
    }

    private void MapActions() {

        new MapStateListener(mMap, mapFragment, getActivity()) {
            @Override
            public void onMapTouched() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMapReleased() {
                LatLng point = mMap.getCameraPosition().target;
                full_address = reverseGeocoding(point.latitude, point.longitude);
                Boolean inside = pointInPolygon(point, polygon);
                if (inside) {
                    locationChangeResult.getAddressLocation(full_address, point.latitude + "", point.longitude + "");
                    progress.setVisibility(View.GONE);
                } else {
                    getWaring(getString(R.string.riyadh_support), 1);
                    progress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onMapUnsettled() {
                // Map unsettled
            }

            @Override
            public void onMapSettled() {
                // Map settled
            }
        };

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userCurrentLocation != null) {
                    MarkerOptions currentUserLocation = new MarkerOptions();
                    LatLng currentUserLatLang = new LatLng(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude());
                    currentUserLocation.position(currentUserLatLang);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentUserLatLang, 17));
                }

            }
        });


    }
    Location userCurrentLocation = null;
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
             userCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (userCurrentLocation != null) {
                MarkerOptions currentUserLocation = new MarkerOptions();
                LatLng currentUserLatLang = new LatLng(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude());
                currentUserLocation.position(currentUserLatLang);
                //mMap.addMarker(currentUserLocation);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentUserLatLang, 17));
                if (lat != 0 && lng != 0) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 17));

                }

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Toast.makeText(getActivity(), "ssssssssss", Toast.LENGTH_SHORT).show();

        ;


    }


    public boolean pointInPolygon(LatLng point, Polygon polygon) {
        int crossings = 0;
        List<LatLng> path = polygon.getPoints();
        path.remove(path.size() - 1);
        for (int i = 0; i < path.size(); i++) {
            LatLng a = path.get(i);
            int j = i + 1;
            if (j >= path.size()) {
                j = 0;
            }
            LatLng b = path.get(j);
            if (rayCrossesSegment(point, a, b)) {
                crossings++;
            }
        }
        return (crossings % 2 == 1);
    }

    public static boolean rayCrossesSegment(LatLng point, LatLng a, LatLng b) {
        // Ray Casting algorithm checks, for each segment, if the point is 1) to the left of the segment and 2) not above nor below the segment. If these two conditions are met, it returns true
        double px = point.longitude,
                py = point.latitude,
                ax = a.longitude,
                ay = a.latitude,
                bx = b.longitude,
                by = b.latitude;
        if (ay > by) {
            ax = b.longitude;
            ay = b.latitude;
            bx = a.longitude;
            by = a.latitude;
        }
        // alter longitude to cater for 180 degree crossings
        if (px < 0 || ax < 0 || bx < 0) {
            px += 360;
            ax += 360;
            bx += 360;
        }
        // if the point has the same latitude as a or b, increase slightly py
        if (py == ay || py == by) py += 0.00000001;


        // if the point is above, below or to the right of the segment, it returns false
        if ((py > by || py < ay) || (px > Math.max(ax, bx))) {
            return false;
        }
        // if the point is not above, below or to the right and is to the left, return true
        else if (px < Math.min(ax, bx)) {
            return true;
        }
        // if the two above conditions are not met, you have to compare the slope of segment [a,b] (the red one here) and segment [a,p] (the blue one here) to see if your point is to the left of segment [a,b] or not
        else {
            double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Double.POSITIVE_INFINITY;
            double blue = (ax != px) ? ((py - ay) / (px - ax)) : Double.POSITIVE_INFINITY;
            return (blue >= red);
        }

    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            locationChangeResult = (LocationChangeResult) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LogoutUser");
        }
    }


    public LatLng getLocationFromAddress(String strAddress, Context
            context) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng((double) (location.getLatitude() * 1E6),
                    (double) (location.getLongitude() * 1E6));
            mMap.clear();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(p1));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(p1, 16));
            return p1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p1;
    }


    public void setMyHomeLocation(boolean b) {
        setLocationPosition();
        mMap.setMyLocationEnabled(b);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

    }
}