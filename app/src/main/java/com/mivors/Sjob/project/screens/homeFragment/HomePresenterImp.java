package com.mivors.Sjob.project.screens.homeFragment;

import android.content.Context;
import android.content.Intent;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.helpers.Categories;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.screens.createServiceRequestActivity.CreateServiceRequest;

import javax.inject.Inject;

/**
 * Created by Ahmed shaban on 7/31/2017.
 */

public class HomePresenterImp implements HomePresenter {
    HomeView  view;
    Intent i ;

    @Inject
    Context context;

    @Override
    public void setView(HomeView view) {
        this.view  = view;
        i = new Intent(context, CreateServiceRequest.class);
    }



    public HomePresenterImp(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
    }

    @Override
    public void RequestAirConditioning() {
        i.putExtra(Tokens.CATEGORY_TYPE, Categories.aircondition.getValue());
        i.putExtra(Tokens.CATEGORY_NAME,  context.getString(R.string.airCond));
         view.launchAirConditioning(i);
    }

    @Override
    public void RequestPlumbing() {
        i.putExtra(Tokens.CATEGORY_TYPE, Categories.plumbing.getValue());
        i.putExtra(Tokens.CATEGORY_NAME, context.getString(R.string.plumb));
        view.launchAirConditioning(i);

    }

    @Override
    public void RequestElectrical() {
        i.putExtra(Tokens.CATEGORY_TYPE, Categories.electrical.getValue());
        i.putExtra(Tokens.CATEGORY_NAME,  context.getString(R.string.elec));
        view.launchAirConditioning(i);

    }

    @Override
    public void RequestSatelite() {
        i.putExtra(Tokens.CATEGORY_TYPE, Categories.satellite.getValue());
        i.putExtra(Tokens.CATEGORY_NAME,  context.getString(R.string.satellite));
        view.launchSatellite(i);
    }

    @Override
    public void RequestMobile() {
        i.putExtra(Tokens.CATEGORY_TYPE, Categories.mobile.getValue());
        i.putExtra(Tokens.CATEGORY_NAME,  context.getString(R.string.mobile));
        view.launchMobile(i);
    }

    @Override
    public void RequestCleaning() {
        i.putExtra(Tokens.CATEGORY_TYPE, Categories.cleaning.getValue());
        i.putExtra(Tokens.CATEGORY_NAME,  context.getString(R.string.cleaning));
        view.launchCleaning(i);
    }
}
