package com.mivors.Sjob.project.screens.createServiceRequestActivity;

import android.content.Context;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public interface CreateServiceRequestPresenter {
    void setView(CreateServiceRequestView view, Context context);
    void RequestSubmitServiceRequest(int payment, int severity, int visitIndex, String description, String location, String locationDetails, int contact_id,int categoryId,String lat,String lng);
    void RequestClock();
    void RequestCalender();

    void changeVisitStatus(int position);
}
