package com.mivors.Sjob.project.screens.registerActivity;

import android.content.Context;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

public interface RegisterPresenter {
    void setView(RegisterView view);
    void RequestTitle();
    boolean inputData(String email, String pass, String rePass, Context context);
    void checkEmail(String email);

}
