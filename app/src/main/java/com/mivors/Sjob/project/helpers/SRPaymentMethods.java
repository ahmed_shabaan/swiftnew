package com.mivors.Sjob.project.helpers;

/**
 * Created by ahmed on 17/09/17.
 */



public enum SRPaymentMethods {
    Cash(31), Mada(34);
    private final int value;

    private SRPaymentMethods(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

