package com.mivors.Sjob.project.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.mivors.Sjob.project.helpers.Tokens.DATE_TIME_FORMAT_DISPLAY_FORMAT;
import static com.mivors.Sjob.project.helpers.Tokens.DATE_TIME_FORMAT_RIGHT_NOW;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public class DateAndTimeOperations {
    Calendar calander;

    public DateAndTimeOperations() {
        calander = Calendar.getInstance();

    }


    public String setRNDdateFormat (String date, String time) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(Tokens.DATE_TIME_FORMAT,new Locale("en"));
        SimpleDateFormat toFormat = new SimpleDateFormat(DATE_TIME_FORMAT_RIGHT_NOW, new Locale("en"));
        toFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            String reformattedStr = toFormat.format(fromFormat.parse(date + " " + time));
            return reformattedStr;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getCurrentDate () {
        int cDay = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH)+1;
        int cYear = calander.get(Calendar.YEAR);
        return (cYear+"-"+cMonth+"-"+cDay);
    }

    public String getCurrentTime () {
        Calendar calendar = Calendar.getInstance();
        int cHour = calendar.get(Calendar.HOUR_OF_DAY);
        int cMinute = calendar.get(Calendar.MINUTE);
        int cSecond = calendar.get(Calendar.SECOND);
        return (cHour+":"+cMinute+":"+cSecond);
    }


    public String getCurrentDataWithTimeZone(String date_string) throws ParseException {
        android.text.format.DateFormat.format("yyyy-MM-dd'T'HH:mm:ssZ", new java.util.Date());
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT_RIGHT_NOW,new Locale("en"));
        String dateInString = date_string;
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = formatter.parse(dateInString);
        SimpleDateFormat currentFormat = new SimpleDateFormat(DATE_TIME_FORMAT_RIGHT_NOW ,new Locale("en"));
        TimeZone current_time = TimeZone.getDefault();
        currentFormat.setTimeZone(current_time);
        String convertedDate = currentFormat.format(date);
        // Convert to String first
        return convertedDate;
    }

    public String getCurrentDataWithTimeZoneForCompare(String date_string)  {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT_RIGHT_NOW,new Locale("en"));
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try {
            date = formatter.parse(date_string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat currentFormat = new SimpleDateFormat(DATE_TIME_FORMAT_RIGHT_NOW ,new Locale("en"));
        TimeZone current_time = TimeZone.getDefault();
        currentFormat.setTimeZone(current_time);
        String convertedDate = currentFormat.format(date);
        // Convert to String first
        return convertedDate;
    }

    public   String convertToTwentyFourHours(String input) {
        DateFormat inputFormat = new SimpleDateFormat("hh:mm:ss a",new Locale("en"));
        DateFormat outFormat = new SimpleDateFormat("HH:mm:ss",new Locale("en"));
        String dateWithTimeZone = null;
        try {
            dateWithTimeZone = outFormat.format(inputFormat.parse(input));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateWithTimeZone;
    }


    public String getDateForDisplay(String input) {
        DateFormat inputFormat = new SimpleDateFormat(Tokens.DATE_TIME_FORMAT_RIGHT_NOW);
        DateFormat outputFormat = new SimpleDateFormat(DATE_TIME_FORMAT_DISPLAY_FORMAT);
        try {
            String dateWithTimeZone = getCurrentDataWithTimeZone(input);
            dateWithTimeZone = outputFormat.format(inputFormat.parse(dateWithTimeZone));
            return  dateWithTimeZone;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getDateForDisplayEdit(String input) {
        DateFormat inputFormat = new SimpleDateFormat(Tokens.DATE_TIME_FORMAT_RIGHT_NOW,new Locale("en"));
        DateFormat outputFormat = new SimpleDateFormat(DATE_TIME_FORMAT_DISPLAY_FORMAT,new Locale("en"));
        try {
            String dateWithTimeZone = getCurrentDataWithTimeZone(input);
            dateWithTimeZone = outputFormat.format(inputFormat.parse(dateWithTimeZone));
            return  dateWithTimeZone;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
