package com.mivors.Sjob.project.screens.loginActivity;

/**
 * Created by ahmed radwan on 7/31/2017.
 */

public interface LoginView {
    void showLoader();
    void hideLoader();
    void edEmail();
    void edPassword();
    void chRemember();
    void chKeepMeLogin();
    void btnLogin();
    void imgPassView();
    void txtForgetPass();
    void requestLogin(String s);
    void requestWaring(String string);

}
