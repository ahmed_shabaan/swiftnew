package com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Incident.IncidentCallBacks;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.serviceRequest.ServicesRequestParser;

import org.json.JSONException;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

import static com.mivors.Sjob.project.screens.showServicesRequestsFragment.ServiceRequestAdapter.context;

/**
 * Created by Ahmed shaban on 8/9/2017.
 */

public class ShowServicesRequestInfoImpl implements ShowServicesRequestInfoPresenter , IncidentCallBacks{
    ShowServicesRequestInfoView view;
    @Inject
    ApiManager apiManager;
    @Inject
    Context context;
    @Override
    public void setView(ShowServicesRequestInfoView view) {
        this.view  = view;
    }

    public ShowServicesRequestInfoImpl(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
    }

    @Override
    public void RequestServicesRequest(String id) {
        view.showLoader();
        apiManager.RegisterIncidentCallback(this);
        String query = "SELECT C.CustomFields.c.c_product , C.ID , C.CustomFields.c.severity_level , C.CustomFields.c.description , Subject , C.CustomFields.c.gpslocation  ,C.CustomFields.c.house_description , " +
                "C.CustomFields.c.date ,C.CustomFields.TOA.city , C.CustomFields.TOA.street, Incidents.StatusWithType.Status " +
                ", C.CustomFields.c.technician_name , C.CustomFields.c.technician_rate ,  C.CustomFields.c.service_rate , C.ReferenceNumber" +
                " , C.CustomFields.c.cost , C.CustomFields.c.complain_title ,"
                + "C.CustomFields.c.customer_feedback , C.CustomFields.c.complain_start_time ," +
                "C.CustomFields.c.complain_closed_time ,C.CustomFields.c.complaint_status , C.CustomFields.TOA.coordinate_x,C.CustomFields.TOA.coordinate_y , C.CustomFields.c.sr_payment_method"+
                " From Incidents  C  WHERE  C.ID="+id;
        apiManager.GetQueryResult(query, GeneralTypesEnum.incidents.name());
    }

    @Override
    public void CreateIncident(String result, int code) {

    }

    @Override
    public void UpdateIncident(String result, int code) {

    }

    @Override
    public void GetIncidentByID(String result, int code) {

    }

    @Override
    public void GetIncidentQueryResult(String result, int code) {
        view.hideLoader();
        if(code==200){
            try {
                view.requestServicesRequest(ServicesRequestParser.ParseServiceRequests(result));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(code==0) {
            view.requestWaring(context.getString(R.string.internet_error_connection));
            Utility.setCrashlyticsLog(code,"",context.getString(R.string.internet_error_connection),"ShowServicesRequestInfo");


        }else {
            view.requestWaring(context.getString(R.string.sms_send_error));
           Utility.setCrashlyticsLog(code,result,context.getString(R.string.sms_send_error),"ShowServicesRequestInfo");

        }
    }
}
