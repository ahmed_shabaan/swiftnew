package com.mivors.Sjob.project.screens.forgetActivity;

/**
 * Created by ahmed radwan on 7/31/2017.
 */

public interface ForgetPassView {
    void showLoader();
    void hideLoader();
    void edEmail();
    void btnForgetPass();
    void requestTitle(String s);

}
