package com.mivors.Sjob.project.screens.about_usFragment;

import android.content.Context;
import android.content.Intent;

 import com.mivors.Sjob.project.helpers.DaggerApplication;
 
import javax.inject.Inject;

/**
 * Created by Ahmed shaban on 7/31/2017.
 */

public class AboutUsPresenterImp implements AboutUsPresenter {
    AboutUsView view;
    Intent i ;

    @Inject
    Context context;

    @Override
    public void setView(AboutUsView view) {
        this.view  = view;
     }

    public AboutUsPresenterImp(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
    }


}
