package com.mivors.Sjob.project.screens.editAccount;

/**
 * Created by ahmed radwan on 8/3/2017.
 */

public interface EditAccountView {
    void showLoader();
    void hideLoader();
    void btnEditAccount();
    void spinnerView();
    void requestData(String string);
    void edAddressTitle();
    void edAddressDetails();
    void edPass();
    void edRePass();
    void edFirstName();
    void edLastName();
    void edPhone();
    void requestWaring(String string);

}
