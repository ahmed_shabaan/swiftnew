package com.mivors.Sjob.project.screens.createServiceRequestActivity;

import android.app.DatePickerDialog;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.RNClasses.Incident.Incident;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.callBacks.LocationChangeResult;
import com.mivors.Sjob.project.helpers.Categories;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.screens.mapFragment.MainMapFragment;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

public class CreateServiceRequest extends BaseActivity implements LocationChangeResult, CreateServiceRequestView {
    MainMapFragment mainMapFragment;
    FragmentManager fragmentManager;
    String location = null;

    @Bind(R.id.payment_method_spinner)
    MaterialSpinner paymentMethodsSpinner;

    @Bind(R.id.severity_level_spinner)
    MaterialSpinner severityLevelSpinner;

    @Bind(R.id.visit_time_spinner)
    MaterialSpinner visitTimeSpinner;

    @Bind(R.id.dateET)
    EditText dateEt;

    @Bind(R.id.timeET)
    EditText timeET;

    @Bind(R.id.descriptionET)
    EditText descriptionET;

    @Bind(R.id.locationET)
    EditText locationEt;

    @Bind(R.id.locationDetailsET)
    EditText locationDetailsET;

    @Bind(R.id.submit)
    Button submit;

    @Bind(R.id.visit_time_layout)
    LinearLayout visit_time_layout;

    Boolean hasPermission = true;


    @Inject
    CreateServiceRequestPresenter createServiceRequestPresenter;

    String[] payMethods = null;
    String[] severityArray = null;
    String[] visitTimeArray = null;
    ArrayAdapter<String> paymentMethodAdapter, severityAdapter, visitTimeAdapter;
    int serviceType;
    String visit = "",futureDate ="";
    String lat,lng;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_service_request);
        ButterKnife.bind(this);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);
        String title = getIntent().getStringExtra(Tokens.CATEGORY_NAME);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        createServiceRequestPresenter.setView(this, this);
        mainMapFragment = new MainMapFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.map_frame_layout, mainMapFragment, "mainMapFragment").commit();

        serviceType = getIntent().getExtras().getInt(Tokens.CATEGORY_TYPE);
        payMethods = getResources().getStringArray(R.array.paymentMethods_array);
        paymentMethodAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, payMethods);
        paymentMethodAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        paymentMethodsSpinner.setAdapter(paymentMethodAdapter);


        severityArray = getResources().getStringArray(R.array.severity_array);
        severityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, severityArray);
        severityAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        severityLevelSpinner.setAdapter(severityAdapter);


        visitTimeArray = getResources().getStringArray(R.array.prefVisit_array);
        visitTimeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, visitTimeArray);
        visitTimeAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        visitTimeSpinner.setAdapter(visitTimeAdapter);


        dateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServiceRequestPresenter.RequestCalender();
            }
        });

        timeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServiceRequestPresenter.RequestClock();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if("future".equals(visit)){
                        getWaring(getResources().getString(R.string.last_time), 0);

                    } else if("future_date".equals(futureDate)) {
                    getWaring(getResources().getString(R.string.last_time), 0);

                }

                    else
                    createServiceRequestPresenter.RequestSubmitServiceRequest(
                            paymentMethodsSpinner.getSelectedItemPosition()
                            , severityLevelSpinner.getSelectedItemPosition()
                            , visitTimeSpinner.getSelectedItemPosition(),
                            Utility.solveCharacter(descriptionET.getText().toString()),
                            locationEt.getText().toString(),
                            Utility.solveCharacter(locationDetailsET.getText().toString()),
                            prefManager.getId(),
                            serviceType,lat,lng);


            }
        });


        visitTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                createServiceRequestPresenter.changeVisitStatus(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void getAddressLocation(String location,String lat,String lng) {
        locationEt.setText(location);
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public void submitServiceRequest() {

    }

    @Override
    public void updateDate(String s) {

        if(s.equalsIgnoreCase("future_date")) {
            getWaring(getResources().getString(R.string.last_date), 0);
            dateEt.setText("");
            futureDate = "future_date";
        }else {
            dateEt.setText(s);
            futureDate = "";
        }

    }

    @Override
    public void updateTime(String s) {

        if(s.equalsIgnoreCase("future")){
            getWaring(getResources().getString(R.string.last_time),0);
            timeET.setText("");
            visit="future";
        }else {
            timeET.setText(s);
            visit="";
        }
    }

    @Override
    public void requestWarring(String s) {
        getWaring(s, 1);
    }

    @Override
    public void showSuccess(String s) {
        getSuccess(s, Tokens.CREATE_SERVICE_REQUEST_SUCCESS);
    }

    @Override
    public void hidLoader() {
        hideProgress();
    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideVisitSpinner() {
        visit_time_layout.setVisibility(View.GONE);
    }

    @Override
    public void showVisitSpinner() {
        visit_time_layout.setVisibility(View.VISIBLE);

    }

    @Override
    public void showLoginNeed() {
        showRegister();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mainMapFragment.setMyHomeLocation(true);
        } else {
           hasPermission = false;
        }
    }
}
