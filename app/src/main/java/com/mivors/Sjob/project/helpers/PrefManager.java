package com.mivors.Sjob.project.helpers;

/**
 * Created by Yehia Fathi on 9/26/2016.
 */
import android.content.Context;
import android.content.SharedPreferences;

import com.mivors.Sjob.project.model.User;


public class PrefManager {
    private  SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private String username="username";
    private String password ="password";
    private String login ="login";
    private String rememberMe ="remember";
    private String addressTitle = "address_title";
    private String addressDetails = "address_details";
    private String phone = "phone";
    private String title = "title";
    private String firstName = "first_name";
    private String lastName = "last_name";
    private String id = "id";
    private String titleId = "title_id";
    private String password_saved = "password_saved";

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "prefname";
    private static final String USER_ID = "ID";
    private static final String LOGGED_IN = "logged_in";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private String lat="lat",lng="lng";

    public  PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean check_login(){
        return  pref.getBoolean(LOGGED_IN,false);
    }

    public  void setLogin(){
        editor.putBoolean(LOGGED_IN,true);
        editor.commit();
    }
    public  void setLogin(boolean bool){
        editor.putBoolean(LOGGED_IN,bool);
        editor.commit();
    }
    public void logout() {
        editor.putBoolean(LOGGED_IN,false);
        setKeepMeLogin(false);
         Utility.LOGIN_ONCE =0;
        editor.commit();
    }
    public void saveUserID(long user_id) {
        editor.putLong(USER_ID, user_id);
        editor.commit();
    }

    public void set_email_password(String email,String password) {
         editor.putString(this.password, password);
        editor.commit();
    }
    public String getEmail(){
        return  pref.getString("email", "");
    }
    public void setEmail(String email) {
        editor.putString("email", email);
         editor.commit();
    }
    public String getUsername(){
        return  pref.getString(username, "");
    }

    public void setPassword(String password) {
         editor.putString(this.password_saved, password);
        editor.commit();
    }
    public void setPasswords(String password) {
        editor.putString(this.password, password);
        editor.commit();
    }
    public String getPasswords(){
        return  pref.getString(password_saved, "");
    }
    public String getPassword(){
        return  pref.getString(password, "");
    }

    public  void  update_language(String lang){
        editor.putString("lang",lang);
        editor.commit();

    }
    public  String  get_lang(){
        return  pref.getString("lang", "ar");

    }

    public  String  getLat(){
        return  pref.getString(lat,null);

    }

    public  String  getLng(){
        return  pref.getString(lng,null);

    }

    public boolean getRememberMe(){
        return  pref.getBoolean(rememberMe, false);
    }

    public void setRememberMe(boolean user_id) {
        editor.putBoolean(rememberMe, user_id);
        editor.commit();
    }
    public boolean getKeepMeLogin(){
        return  pref.getBoolean(login, false);
    }

    public void setKeepMeLogin(boolean logins) {
        editor.putBoolean(login, logins);
        editor.commit();
    }
    public String getFirstName(){
        return  pref.getString(firstName, "");
    }

    public String getLastName(){
        return  pref.getString(lastName, "");
    }
    public String getPhone(){
        return  pref.getString(phone, "");
    }
    public String getAddressDetails(){
        return  pref.getString(addressDetails, "");
    }
    public  int  getId(){
        return  pref.getInt(this.id,0);

    }
    public  void  setId(int ids){
        editor.putInt(this.id, ids);
        editor.commit();

    }
    public String getAddressTitle(){
        return  pref.getString(addressTitle, "");
    }
    public String getTitle(){
        return  pref.getString(title, "");
    }
    public void set_data(User user) {
         editor.putString(this.firstName, user.getFirstName());
        editor.putString(this.lastName, user.getLastName());
        editor.putString(this.addressTitle, user.getAddressTitle());
        editor.putString(this.addressDetails, user.getAddressDetails());
        editor.putString(this.password, user.getPassword());
        editor.putString(this.title, user.getTitle());
        editor.putString(this.phone, user.getPhone());
        editor.putString(this.lat, user.getLat());
        editor.putString(this.lng, user.getLng());

          editor.commit();
    }


    public int getTitleId() {
        return  pref.getInt(this.titleId,0);
    }
    public  void  setTitleId(int ids){
        editor.putInt(this.titleId, ids);
        editor.commit();

    }
    public int getUserID() {
        return  pref.getInt(this.id,0);
    }
}
