package com.mivors.Sjob.project.screens.testMvpModel;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.DaggerApplication;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */



public class TestMvpActivity extends BaseActivity implements TestView {

    @Inject
    testPresenter  testPresenter;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.progress)
    ProgressBar progressBar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((DaggerApplication)getApplication()).getAppComponent().inject(this);
        testPresenter.setView(this);
        testPresenter.RequestTitle();



    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideLoader() {
       hideProgress();

    }

    @Override
    public void requestTitle(String s) {
        //getSuccess("sucess",1);
        title.setText(s);
    }
}
