package com.mivors.Sjob.project.screens.showServicesRequestsFragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.helpers.DateAndTimeOperations;
import com.mivors.Sjob.project.helpers.Statues;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static com.mivors.Sjob.project.helpers.Tokens.DATE_TIME_FORMAT_DISPLAY_FORMAT;



public class ServiceRequestAdapter  extends RecyclerView.Adapter<ServiceRequestAdapter.ViewHolder> {

    private List<ServiceRequest> dataSet;
    public static Context context;
    DateAndTimeOperations dateAndTimeOperations;


    public ServiceRequestAdapter(List<ServiceRequest> os_versions, Context context) {

        dataSet = os_versions;
        this.context = context;
        dateAndTimeOperations  = new DateAndTimeOperations();
    }


    @Override
    public ServiceRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.service_item, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ServiceRequestAdapter.ViewHolder viewHolder,  int i) {

        ServiceRequest fp = dataSet.get(i);
        String input  = null;
        //bind title
        viewHolder.title.setText(fp.getTitle());


        //bind date and time
        DateFormat inputFormat = new SimpleDateFormat(Tokens.DATE_TIME_FORMAT_RIGHT_NOW);
        DateFormat outputFormat = new SimpleDateFormat(DATE_TIME_FORMAT_DISPLAY_FORMAT);

        try {
            String dateWithTimeZone = dateAndTimeOperations.getCurrentDataWithTimeZone(fp.getDate());
            dateWithTimeZone = outputFormat.format(inputFormat.parse(dateWithTimeZone));
            viewHolder.date.setText(dateWithTimeZone);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //bind status
        if (fp.getIncidentStatues() == Statues.canceled.getValue()) {
            viewHolder.status.setImageResource(R.drawable.redcircle);
            viewHolder.rightStatus.setBackgroundColor(context.getResources().getColor(R.color.canceled));
        } else  if (fp.getIncidentStatues() == Statues.pending.getValue()){
            viewHolder.status.setImageResource(R.drawable.pendingcircle);
            viewHolder.rightStatus.setBackgroundColor(context.getResources().getColor(R.color.pending));
        }
        else{
            viewHolder.status.setImageResource(R.drawable.greencircle);
            viewHolder.rightStatus.setBackgroundColor(context.getResources().getColor(R.color.completed));
        }

        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView date;
        public ServiceRequest feed;
        ImageView status;
        ImageView rightStatus;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title = (TextView) itemLayoutView
                    .findViewById(R.id.srid);
            date = (TextView) itemLayoutView
                    .findViewById(R.id.srdate);
            status = (ImageView) itemLayoutView
                    .findViewById(R.id.cicrleindecator);
            rightStatus = (ImageView) itemLayoutView
                    .findViewById(R.id.rectangel);




            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle  b = new Bundle();
                    b.putParcelable("item",feed);
                    ((Activity) context).startActivityForResult(new Intent(v.getContext(), ShowServicesRequestInfoActivity.class).putExtras(b),Tokens.UPDATE_SR);

                }


            });




        }

    }
}



