package com.mivors.Sjob.project.screens.fullRegister;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.RightNow.RNObject;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.loginActivity.LoginView;

import org.json.JSONException;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

/**
 * Created by ahmed radwan on 8/3/2017.
 */

public class FullRegisterPresenterImpl implements FullRegisterPresenter,ContactCallBacks {

    FullRegisterView fullRegisterView;
    @Inject
    ApiManager apiManager;

    @Inject
    PrefManager prefManager;
//    @Inject
//    Utility utility;

    @Inject
    Context context;

    public FullRegisterPresenterImpl(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
    }

    @Override
    public void CreateContact(String result, int code) {
        fullRegisterView.hideLoader();
        if(code==200||code==201) {
            try {
                Utility utility = new Utility(prefManager);
                String pass = utility.jsonRegister(result);
                fullRegisterView.requestData(pass);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else if(code ==0){
            fullRegisterView.requestWaring(context.getString(R.string.internet_error_connection));
            Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
                    ,"Register");
        }
        else {
            fullRegisterView.requestWaring(context.getString(R.string.sms_send_error));
            Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"Register");

        }


    }

    @Override
    public void UpdateContact(String result, int code) {

    }

    @Override
    public void GetContactByID(String result, int code) {

    }

    @Override
    public void GetContactQueryResult(String result, int code) {

    }

    @Override
    public void setView(FullRegisterView fullRegisterView) {
        this.fullRegisterView = fullRegisterView;
        fullRegisterView.spinnerView();
        fullRegisterView.btnCreateAccount();
    }

    @Override
    public void inputData(User user) {

            fullRegisterView.showLoader();
            apiManager.RegisterContactCallback(this);
            RNObject rnObject = new RNObject(GeneralTypesEnum.contacts.name());
            prefManager.set_data(user);
            prefManager.setTitleId(user.getTitleId());
            prefManager.setId(user.getId());

            String data = null;
            try {
                 data = Utility.jsonCreate(user);
                apiManager.Create(rnObject,data);
             } catch (JSONException e) {
                e.printStackTrace();

        }
    }


}
