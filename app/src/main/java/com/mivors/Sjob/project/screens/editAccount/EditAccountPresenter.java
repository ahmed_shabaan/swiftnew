package com.mivors.Sjob.project.screens.editAccount;

import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.fullRegister.FullRegisterView;

/**
 * Created by ahmed radwan on 8/3/2017.
 */

public interface EditAccountPresenter {

    void setView(EditAccountView fullRegisterView);
     void inputData(User user);
}
