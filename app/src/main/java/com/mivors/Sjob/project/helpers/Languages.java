package com.mivors.Sjob.project.helpers;

/**
 * Created by Ahmed shaban on 7/31/2017.
 */

public enum Languages {
    ARABIC("ar"), ENGLISH("en");
    private final String value;

    private Languages(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
