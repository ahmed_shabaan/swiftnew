package com.mivors.Sjob.project.screens.loginActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassword;
import com.mivors.Sjob.project.screens.homeActivity.home;

import org.json.JSONException;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Login extends BaseActivity implements LoginView {

    @Inject
    LoginPresenter loginPresenter;

    @Inject
    PrefManager prefManager;

    @Bind(R.id.edEmail)
    EditText edEmail;

    @Bind(R.id.edPass)
    EditText edPass;

    @Bind(R.id.chRememberMe)
    CheckBox chRememberMe;

    @Bind(R.id.chKeepMe)
    CheckBox chKeepMe;

    @Bind(R.id.btnLogin)
    Button btnLogin;
    @Bind(R.id.imgPassView)
    ImageView imgPassView;
    @Bind(R.id.tvForgetPass)
    TextView tvForgetPass;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        ((DaggerApplication)getApplication()).getAppComponent().inject(this);

         loginPresenter.setView(this);
        loginPresenter.showPass();
        loginPresenter.forgetAction();
        loginPresenter.checkLogin();
        loginPresenter.checkRemember();


        if(prefManager.getRememberMe()){
          chRememberMe.setChecked(true);

            edEmail.setText(prefManager.getEmail());
            edPass.setText(prefManager.getPasswords());
         }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.title_activity_login);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void showLoader() {
        showProgress();

    }

    @Override
    public void hideLoader() {
        hideProgress();

    }

    @Override
    public void edEmail() {

    }

    @Override
    public void edPassword() {

    }

    @Override
    public void chRemember() {
        chRememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    prefManager.setRememberMe(true);
                    prefManager.set_email_password(edEmail.getText().toString(),edPass.getText().toString());

                }else{
                    prefManager.setRememberMe(false);
                    prefManager.set_email_password("","");
                }
            }
        });

    }

    @Override
    public void chKeepMeLogin() {
        chKeepMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    prefManager.setKeepMeLogin(true);
                }else{
                    prefManager.setKeepMeLogin(false);
                }

            }
        });


    }

    @Override
    public void btnLogin() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edEmail.getText().toString().equals("")){
                    getWaring(getResources().getString(R.string.empty_email),0);

                }else if(!Utility.emailValidates(edEmail.getText().toString())){
                    getWaring(getResources().getString(R.string.check_email_syntax),0);

                } else if ("".equals(edPass.getText().toString())){
                    getWaring(getResources().getString(R.string.empty_pass),0);

                }
//
//                else if(!Utility.checkCharacter(edPass.getText().toString())||
//                        !Utility.checkCharacter(edEmail.getText().toString()))
//                {
//                    getWaring(getResources().getString(R.string.check_email_syntax), 0);
//
//                }

                else {
                    loginPresenter.inputData(Utility.solveCharacter(edEmail.getText().toString())
                            ,Utility.solveCharacter(edPass.getText().toString()));

                }

             }
        });
    }

    @Override
    public void imgPassView() {
        imgPassView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edPass.getTransformationMethod()==null){
                    edPass.setTransformationMethod(new PasswordTransformationMethod());
                 }else{
                     edPass.setTransformationMethod(null);

                }
            }

        });
    }

    @Override
    public void txtForgetPass() {
        tvForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, ForgetPassword.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void requestLogin(String s) {
        if( s.equals(edPass.getText().toString())){
       //  getSuccess(getResources().getString(R.string.success_login), Tokens.LOGIN_ACTION);
            setResult(Tokens.LOGIN_ACTION);

            Toast.makeText(this, getResources().getString(R.string.success_login), Toast.LENGTH_SHORT).show();
            finish();
            Utility.LOGIN_ONCE = 1;
            prefManager.setPassword(edPass.getText().toString());
            prefManager.setPasswords(edPass.getText().toString());

            logUser();
            if( prefManager.getRememberMe()) {
                prefManager.set_email_password(edEmail.getText().toString(), edPass.getText().toString());
                prefManager.setEmail(edEmail.getText().toString());
            }
            if( prefManager.getKeepMeLogin()) {
                prefManager.setLogin();

            }

        }else {
            getWaring(getResources().getString(R.string.wrong_email_or_pass),0);
        }
    }

    @Override
    public void requestWaring(String string) {
        getWaring(string,1);

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods



         Crashlytics.setUserEmail(edEmail.getText().toString());

     }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
