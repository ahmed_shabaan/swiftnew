package com.mivors.Sjob.project.screens.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.loginActivity.Login;
import com.mivors.Sjob.project.screens.registerActivity.Register;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ahmed radwan on 8/9/2017.
 */

public class SettingsFragment extends Fragment {

    @Bind(R.id.btnLogin)
    Button btnLogin;

    @Bind(R.id.btnRegister)
    Button btnRegister;

    @Bind(R.id.btnLogout)
    Button btnLogout;
    Intent intent;

    @Inject
    PrefManager prefManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((DaggerApplication)getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((DaggerApplication)getActivity().getApplication()).getAppComponent().inject(this);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.settings_fragment, container, false);
        ButterKnife.bind(this,view);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!=null){
            actionBar.setTitle(R.string.settings);

        }

        declare();
        if(prefManager.check_login()|| Utility.LOGIN_ONCE==1){
            btnLogin.setVisibility(View.GONE);
            btnRegister.setVisibility(View.GONE);
            btnLogout.setVisibility(View.VISIBLE);
        }else  if(prefManager.check_login()&& prefManager.getKeepMeLogin()){
            btnLogin.setVisibility(View.GONE);
            btnRegister.setVisibility(View.GONE);
            btnLogout.setVisibility(View.VISIBLE);
        }else
            {
            btnLogin.setVisibility(View.VISIBLE);
            btnRegister.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.GONE);
        }
        ((DaggerApplication)getActivity().getApplication()).getAppComponent().inject(this);

        return view;
    }

    private void declare(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Login.class);
                startActivityForResult(i, Tokens.LOGIN_ACTION);


            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), Register.class);
                startActivityForResult(i, Tokens.LOGIN_ACTION);

            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefManager.logout();
                getActivity().recreate();
                Utility.LOGIN_ONCE=0;
                prefManager.set_data(new User());
                prefManager.setLogin(false);


            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(prefManager.check_login()||Utility.LOGIN_ONCE==1){
            btnLogin.setVisibility(View.GONE);
            btnRegister.setVisibility(View.GONE);
            btnLogout.setVisibility(View.VISIBLE);
        }else{
            btnLogin.setVisibility(View.VISIBLE);
            btnRegister.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.GONE);
        }    }


}
