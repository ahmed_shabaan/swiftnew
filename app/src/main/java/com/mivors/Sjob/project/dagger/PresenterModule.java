package com.mivors.Sjob.project.dagger;

import android.content.Context;

import com.mivors.Sjob.project.helpers.DateAndTimeOperations;
import com.mivors.Sjob.project.screens.EditServiceRequest.UpdateServiceRequestPresenter;
import com.mivors.Sjob.project.screens.EditServiceRequest.UpdateServiceRequestPresenterImp;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoImpl;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoPresenter;
import com.mivors.Sjob.project.screens.about_usFragment.AboutUsPresenter;
import com.mivors.Sjob.project.screens.about_usFragment.AboutUsPresenterImp;
import com.mivors.Sjob.project.screens.createServiceRequestActivity.CreateServiceRequestPresenter;
import com.mivors.Sjob.project.screens.createServiceRequestActivity.CreateServiceRequestPresenterImp;
import com.mivors.Sjob.project.screens.editAccount.EditAccountPresenter;
import com.mivors.Sjob.project.screens.editAccount.EditAccountPresenterImpl;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassPresenter;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassPresenterImpl;
import com.mivors.Sjob.project.screens.forgetActivity.restPassword.RestPasswordPresenter;
import com.mivors.Sjob.project.screens.forgetActivity.restPassword.RestPasswordPresenterImpl;
import com.mivors.Sjob.project.screens.fullRegister.FullRegisterPresenter;
import com.mivors.Sjob.project.screens.fullRegister.FullRegisterPresenterImpl;
import com.mivors.Sjob.project.screens.homeFragment.HomePresenter;
import com.mivors.Sjob.project.screens.homeFragment.HomePresenterImp;
import com.mivors.Sjob.project.screens.loginActivity.LoginPresenter;
import com.mivors.Sjob.project.screens.loginActivity.LoginPresenterImpl;
import com.mivors.Sjob.project.screens.registerActivity.RegisterPresenter;
import com.mivors.Sjob.project.screens.registerActivity.RegisterPresenterImpl;
import com.mivors.Sjob.project.screens.showServicesRequestsFragment.ServicesRequestsPresenter;
import com.mivors.Sjob.project.screens.showServicesRequestsFragment.ServicesRequestsPresenterImpl;
import com.mivors.Sjob.project.screens.testMvpModel.TestPresenterImpl;
import com.mivors.Sjob.project.screens.testMvpModel.testPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

@Module
public class PresenterModule {


    @Provides
    testPresenter provideTestPresenterImpl(Context context) {
        return new TestPresenterImpl(context);
    }


    @Provides
    HomePresenter provideHomeFragmentPresenterImpl(Context context) {
        return new HomePresenterImp(context);
    }

    @Provides
    CreateServiceRequestPresenter provideCreateServiceRequestPresenterImp(Context context) {
        return new CreateServiceRequestPresenterImp(context);
    }

    @Provides
    AboutUsPresenter provideAboutUsPresenterImp(Context context) {
        return new AboutUsPresenterImp(context);
    }

    @Provides
    ServicesRequestsPresenter provideServicesRequestsPresenterImpl(Context context) {
        return new ServicesRequestsPresenterImpl(context);
    }

    @Provides
    UpdateServiceRequestPresenter provideUpdateServicesRequestsPresenterImpl(Context context) {
        return new UpdateServiceRequestPresenterImp(context);
    }


    @Provides
    @Singleton
    DateAndTimeOperations provideDateAndTimeOperations(Context context) {
        return new DateAndTimeOperations();
    }


    @Provides
    LoginPresenter provideLoginPresenterImpl(Context context) {
        return new LoginPresenterImpl(context);
    }

    @Provides
    RegisterPresenter provideRegisterPresenterImpl(Context context) {
        return new RegisterPresenterImpl(context);
    }

    @Provides
    ForgetPassPresenter provideForgetPassPresenterImpl(Context context) {
        return new ForgetPassPresenterImpl(context);
    }

    @Provides
    FullRegisterPresenter provideFullRegisterPresenterImpl(Context context) {
        return new FullRegisterPresenterImpl(context);
    }

    @Provides
    EditAccountPresenter editAccountPresenterImpl(Context context) {
        return new EditAccountPresenterImpl(context);
    }

    @Provides
    ShowServicesRequestInfoPresenter ProvidesShowServicesRequestInfoImpl(Context context) {
        return new ShowServicesRequestInfoImpl(context);
    }

    @Provides
    RestPasswordPresenter ProvidesRestPasswordPresenter(Context context) {
        return new RestPasswordPresenterImpl(context);
    }



}

