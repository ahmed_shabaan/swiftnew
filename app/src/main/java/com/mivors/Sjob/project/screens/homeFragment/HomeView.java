package com.mivors.Sjob.project.screens.homeFragment;

import android.content.Intent;

/**
 * Created by Ahmed shaban on 7/31/2017.
 */

public interface HomeView {
    void launchAirConditioning(Intent i);
    void launchPlumbing(Intent i);
    void launchElectrical(Intent i);

    void launchSatellite(Intent i);

    void launchMobile(Intent i);

    void launchCleaning(Intent i);
}
