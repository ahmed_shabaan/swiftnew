package com.mivors.Sjob.project.screens.welcomeActivities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.screens.homeActivity.home;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getSupportActionBar().hide();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {

                    Intent i;
                    i = new Intent(StartActivity.this, LanguageSelector.class);
                    startActivity(i);

                    finish();
                } catch (Exception e) {

                }
            }
        }, 1500);

   }
}
