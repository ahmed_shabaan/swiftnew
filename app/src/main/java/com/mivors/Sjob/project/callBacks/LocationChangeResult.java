package com.mivors.Sjob.project.callBacks;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public interface LocationChangeResult {
    void getAddressLocation(String s,String lat,String lng);
}
