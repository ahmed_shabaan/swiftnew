package com.mivors.Sjob.project.screens.filterFragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.helpers.Categories;
import com.mivors.Sjob.project.helpers.Statues;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ahmed shaban on 8/6/2017.
 */

public class FilterFragment extends android.support.v4.app.DialogFragment {
    Button filter,cancel;
    LinearLayout from_date,to_date;
    static TextView from;
    static TextView to;
    static int date_type = 1;
    Spinner category_spinner,status_spinner;
    List<String> categories,status;
    String selected_category = "all",selected_status="all";
    //static int fieldFromDay,fieldFromMonth,fieldFromYear,fieldToDay,fieldToMonth,fieldToYear;
    static String froms,tos;
    ArrayAdapter<String> category_adapter,status_adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_dailog, container);
        getDialog().setTitle(R.string.filter_title);
        selected_category = getString(R.string.all);
        selected_status= getString(R.string.all);
        filter = (Button) view.findViewById(R.id.filter);
        cancel = (Button) view.findViewById(R.id.cancel);
        from_date = (LinearLayout) view.findViewById(R.id.from_date);
        to_date = (LinearLayout) view.findViewById(R.id.to_date);
        from = (TextView) view.findViewById(R.id.from);
        to = (TextView) view.findViewById(R.id.to);
        category_spinner = (Spinner) view.findViewById(R.id.spinner_category);
        status_spinner = (Spinner) view.findViewById(R.id.spinner_status);
        categories  = new ArrayList<>();
        categories.add(getString(R.string.all));
        categories.add(getString(R.string.airCond));
        categories.add(getString(R.string.plumb));
        categories.add(getString(R.string.elec));
        categories.add(getString(R.string.cleaning));
        categories.add(getString(R.string.mobile));
        categories.add(getString(R.string.satellite));



        status  = new ArrayList<>();

        status.add(getString(R.string.all));
        status.add(getString(R.string.completed));
        status.add(getString(R.string.pend));
        status.add(getString(R.string.canceled));

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        froms = year + "-" + (month + 1) + "-" + (day-1);
        tos = year + "-" + (month + 1) + "-" + (day+1);

        from.setText(year + "-" + (month + 1) + "-" + (day));
        to.setText(year + "-" + (month + 1) + "-" + day);



        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyToTarget(Activity.RESULT_OK);
                dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyToTarget(Activity.RESULT_CANCELED);
                dismiss();
            }
        });

        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date_type = 1;
                showDatePickerDialog();
            }
        });

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date_type = 2;
                showDatePickerDialog();
            }
        });


        category_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(category_adapter);
        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_category  = categories.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        status_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, status);
        status_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status_spinner.setAdapter(status_adapter);
        status_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_status = status.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        return view;
    }


    private void notifyToTarget(int code) {
        android.support.v4.app.Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            Intent data  = new Intent();
            data.putExtra("from",froms);
            data.putExtra("to",tos);
            data.putExtra("category",selected_category);
            data.putExtra("status",selected_status);
            targetFragment.onActivityResult(getTargetRequestCode(), code, data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void showDatePickerDialog() {
        android.support.v4.app.DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends android.support.v4.app.DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            if (date_type == 1){
                  froms = year + "-" + (month + 1) + "-" + (day-1);
//                fieldFromDay = day;
//                fieldFromDay = day;
//                fieldFromDay = day;
                from.setText( year + "-" + (month + 1) + "-" + day);
        } else {
                  tos = year + "-" + (month + 1) + "-" + (day+1);
                to.setText(year + "-" + (month + 1) + "-" + day);

            }
        }
    }

}