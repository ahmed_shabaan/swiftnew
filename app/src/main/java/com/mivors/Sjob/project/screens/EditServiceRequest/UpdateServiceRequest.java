package com.mivors.Sjob.project.screens.EditServiceRequest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.maps.model.LatLng;
import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.callBacks.LocationChangeResult;
import com.mivors.Sjob.project.helpers.Categories;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.SRPaymentMethods;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoActivity;
import com.mivors.Sjob.project.screens.homeActivity.home;
import com.mivors.Sjob.project.screens.mapFragment.MainMapFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import fr.ganfra.materialspinner.MaterialSpinner;

public class UpdateServiceRequest extends BaseActivity implements LocationChangeResult,UpdateServiceRequestView {
     MainMapFragment mainMapFragment;
     FragmentManager fragmentManager;
     String location = null;

     @Bind(R.id.payment_method_spinner)
     MaterialSpinner paymentMethodsSpinner;

    @Bind(R.id.cat_method_spinner)
    MaterialSpinner catMethodsSpinner;

     @Bind(R.id.severity_level_spinner)
     MaterialSpinner severityLevelSpinner;

     @Bind(R.id.visit_time_spinner)
     MaterialSpinner visitTimeSpinner;

     @Bind(R.id.dateET)
     EditText dateEt;

     @Bind(R.id.timeET)
     EditText timeET;

     @Bind(R.id.descriptionET)
     EditText descriptionET;

     @Bind(R.id.locationET)
     EditText locationEt;

     @Bind(R.id.locationDetailsET)
     EditText locationDetailsET;

     @Bind(R.id.submit)
     Button submit;

     @Bind(R.id.visit_time_layout)
     LinearLayout visit_time_layout;


    ServiceRequest serviceRequest;
    String visit = "",futureDate ="";
    String lat,lng;


     @Inject
     UpdateServiceRequestPresenter updateServiceRequestPresenter;

     String[] payMethods= null;
     String[] catMethods= null;
     String[] severityArray= null;
     String[] visitTimeArray= null;
     ArrayAdapter<String> paymentMethodAdapter,severityAdapter,visitTimeAdapter ,catMethodAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_service_request);
        ButterKnife.bind(this);
        ((DaggerApplication)getApplication()).getAppComponent().inject(this);
        mainMapFragment = new MainMapFragment();
        serviceRequest = getIntent().getExtras().getParcelable("item");

        updateServiceRequestPresenter.setView(this,this);
        bindData(serviceRequest);
        Bundle b = new Bundle();
        b.putString("lat",serviceRequest.getLat());
        b.putString("lng",serviceRequest.getLng());

        mainMapFragment.setArguments(b);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.map_frame_layout, mainMapFragment, "mainMapFragment").commit();


        payMethods = getResources().getStringArray(R.array.paymentMethods_array);
        paymentMethodAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, payMethods);
        paymentMethodAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        paymentMethodsSpinner.setAdapter(paymentMethodAdapter);




        catMethods = getResources().getStringArray(R.array.category_array);
        catMethodAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, catMethods);
        catMethodAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        catMethodsSpinner.setAdapter(catMethodAdapter);


        severityArray = getResources().getStringArray(R.array.severity_array);
        severityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, severityArray);
        severityAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        severityLevelSpinner.setAdapter(severityAdapter);


        visitTimeArray = getResources().getStringArray(R.array.prefVisit_array);
        visitTimeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, visitTimeArray);
        visitTimeAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        visitTimeSpinner.setAdapter(visitTimeAdapter);


        dateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateServiceRequestPresenter.RequestCalender();
            }
        });

        timeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateServiceRequestPresenter.RequestClock();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(prefManager.check_login()||Utility.LOGIN_ONCE==1){
                    int CatID =0;
                    if(catMethodsSpinner.getSelectedItemPosition()==1)
                    {
                        CatID= Categories.aircondition.getValue();
                    }
                    else  if(catMethodsSpinner.getSelectedItemPosition()==2)
                    {
                        CatID= Categories.plumbing.getValue();
                    }
                       else  if(catMethodsSpinner.getSelectedItemPosition()==3)
                    {
                        CatID= Categories.electrical.getValue();
                    }  else  if(catMethodsSpinner.getSelectedItemPosition()==0)
                    {
                        CatID= 0;
                    } else  if(catMethodsSpinner.getSelectedItemPosition()==4)
                    {
                        CatID= Categories.cleaning.getValue();;
                    }
                    else  if(catMethodsSpinner.getSelectedItemPosition()==5)
                    {
                        CatID= Categories.mobile.getValue();
                    } else  if(catMethodsSpinner.getSelectedItemPosition()==6)
                    {
                        CatID= Categories.satellite.getValue();;
                    }


                    if ( "future".equals(visit)){
                        getWaring(getResources().getString(R.string.last_time), 0);

                    }
                    else if("future_date".equals(futureDate)) {
                        getWaring(getResources().getString(R.string.last_time), 0);


                    }else
                        updateServiceRequestPresenter.RequestSubmitUpdateServiceRequest(serviceRequest.getId(),
                            paymentMethodsSpinner.getSelectedItemPosition()
                            ,severityLevelSpinner.getSelectedItemPosition()
                            ,visitTimeSpinner.getSelectedItemPosition(),
                            Utility.solveCharacter(descriptionET.getText().toString()),
                            Utility.solveCharacter(locationEt.getText().toString()),
                            Utility.solveCharacter(locationDetailsET.getText().toString()),
                            prefManager.getId(),
                            CatID,lat,lng);
                }else{
                   getWaring(getString(R.string.login_first_navigation), Tokens.LOGIN_NAVIGATION);
                }

            }
        });


        visitTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateServiceRequestPresenter.changeVisitStatus(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(serviceRequest.getTitle());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void getAddressLocation(String location,String lat,String lng) {
        locationEt.setText(location);
        this.lat=lat;
        this.lng=lng;

    }

    @Override
    public void submitServiceRequest() {

    }

    @Override
    public void updateDate(String s) {

        if(s.equalsIgnoreCase("future_date")) {
            getWaring(getResources().getString(R.string.last_date), 0);
            dateEt.setText("");
            futureDate = "future_date";
        }else {
            dateEt.setText(s);
            futureDate = "";
        }

    }

    @Override
    public void updateTime(String s) {
        if(s.equalsIgnoreCase("future")){
            getWaring(getResources().getString(R.string.last_time),0);
            timeET.setText("");
            visit="future";
        }else {
            timeET.setText(s);
            visit="";
        }
    }

    @Override
    public void requestWarring(String s) {
        getWaring(s,1);
    }

    @Override
    public void showSuccess(String s) {
         getSuccess(s,Tokens.CREATE_SERVICE_REQUEST_SUCCESS);
      // Utility.LOGIN_ONCE =11;

    }

    @Override
    public void hidLoader() {
        hideProgress();
    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideVisitSpinner() {
        visit_time_layout.setVisibility(View.GONE);
    }

    @Override
    public void showVisitSpinner() {
        visit_time_layout.setVisibility(View.VISIBLE);

    }

    @Override
    public String getDate() {
        return dateEt.getText().toString();
    }

    @Override
    public String getTime() {
        return timeET.getText().toString();
    }


    private void bindData(ServiceRequest serviceRequest) {

        setTitle(serviceRequest.getTitle());
        severityLevelSpinner.setSelection(Tokens.getSevertyID(this,serviceRequest.getSeverityLevel()));


        if(serviceRequest.getPaymentMethod() == SRPaymentMethods.Cash.getValue() )
        paymentMethodsSpinner.setSelection(1);
        else
            paymentMethodsSpinner.setSelection(2);
        visitTimeSpinner.setSelection(2);
        if(serviceRequest.getTitle().equals("Air Conditioning")||"تكييف".equals(serviceRequest.getTitle()))
        {
            catMethodsSpinner.setSelection(1);
        }
        if(serviceRequest.getTitle().equals("Plumbing")||"سباكة".equals(serviceRequest.getTitle()))
        {
            catMethodsSpinner.setSelection(2);
        }
        if(serviceRequest.getTitle().equals("Electrical")||"كهرباء".equals(serviceRequest.getTitle()))
        {
            catMethodsSpinner.setSelection(3);
        }
        if(serviceRequest.getTitle().equals("Cleaning")  || serviceRequest.getTitle().equals("تنظيف") )
        {
            catMethodsSpinner.setSelection(4);
        }
        if(serviceRequest.getTitle().equals("Mobile Maintenance")  || serviceRequest.getTitle().equals("صيانة الجوال"))
        {
            catMethodsSpinner.setSelection(5);
        }
        if(serviceRequest.getTitle().equals("Satellite Maintenance")  || serviceRequest.getTitle().equals("صيانة ستالايت"))
        {
            catMethodsSpinner.setSelection(6);
        }




        String  Date= dateAndTimeOperations.getDateForDisplayEdit(serviceRequest.getDate());
        String[] dateAndTime = Date.split(" ");
        dateEt.setText(dateAndTime[0]);
        timeET.setText(dateAndTime[1]+":"+getString(R.string.seconds)+" "+dateAndTime[2]);
        updateServiceRequestPresenter.setDateAndTime(dateAndTime[0],dateAndTime[1]+":"+getString(R.string.seconds)+" "+dateAndTime[2]);

        // mainMapFragment.reverseGeocoding(point.latitude,point.longitude);
        locationEt.setText(serviceRequest.getStreet());
        if(!serviceRequest.getHouseDescription().equalsIgnoreCase("null")){
            locationDetailsET.setText(serviceRequest.getHouseDescription());
        }

        if(!serviceRequest.getDescription().equalsIgnoreCase("null")){
            descriptionET.setText(serviceRequest.getDescription());
        }
        lat = serviceRequest.getLat();
        lng =serviceRequest.getLng();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mainMapFragment.setMyHomeLocation(true);
        }
    }


}
