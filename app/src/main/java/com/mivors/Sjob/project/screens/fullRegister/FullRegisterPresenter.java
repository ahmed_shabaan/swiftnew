package com.mivors.Sjob.project.screens.fullRegister;

import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.registerActivity.RegisterView;

/**
 * Created by ahmed radwan on 8/3/2017.
 */

public interface FullRegisterPresenter {

    void setView(FullRegisterView fullRegisterView);
     void inputData(User user);
}
