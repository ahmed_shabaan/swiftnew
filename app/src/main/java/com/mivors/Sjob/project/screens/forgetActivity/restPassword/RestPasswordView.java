package com.mivors.Sjob.project.screens.forgetActivity.restPassword;

/**
 * Created by Ahmed shaban on 8/23/2017.
 */

public interface RestPasswordView {
    void  showLoader();
    void  hideLoader();
    void enableViews();
    void requestWaring(int resourceID);
    String getInsertedCode();
    void getSuccess(int resourceID);
}
