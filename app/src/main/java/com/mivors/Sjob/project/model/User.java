package com.mivors.Sjob.project.model;

/**
 * Created by ahmed radwan on 8/3/2017.
 */

public class User {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private String title;
    private String addressTitle;
    private String addressDetails;
    private int titleId;
    String lat;
    String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public User() {
    }

    public User(String firstName, String lastName, String email, String password, String phone, String title, String addressTitle, String addressDetails) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.title = title;
        this.addressTitle = addressTitle;
        this.addressDetails = addressDetails;
    }
    public User(String firstName, String lastName, String email, String password, String phone, String title,int titleId ,String addressTitle, String addressDetails,String lat,String lng) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.title = title;
        this.addressTitle = addressTitle;
        this.addressDetails = addressDetails;
        this.titleId = titleId;
        this.lat = lat;
        this.lng = lng;

    }
    public User(String firstName, String lastName,  String password, String phone, String title,int titleId, String addressTitle, String addressDetails,String lat,String lng) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.phone = phone;
        this.titleId = titleId;
        this.title = title;

        this.addressTitle = addressTitle;
        this.addressDetails = addressDetails;
        this.lat = lat;
        this.lng = lng;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public String getAddressDetails() {
        return addressDetails;
    }

    public void setAddressDetails(String addressDetails) {
        this.addressDetails = addressDetails;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }
}
