package com.mivors.Sjob.project.screens.forgetActivity.restPassword;

/**
 * Created by Ahmed shaban on 8/23/2017.
 */

public interface RestPasswordPresenter {
    void setView(RestPasswordView view);
    Void sendCode(String email);
    Void updatedPassword(String password,String confirmPassword);
}
