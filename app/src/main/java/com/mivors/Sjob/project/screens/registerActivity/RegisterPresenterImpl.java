package com.mivors.Sjob.project.screens.registerActivity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.RightNow.RNObject;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.screens.fullRegister.RegisterFullData;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

/**
 * Created by ahmed radwan on 8/1/2017.
 */

public class RegisterPresenterImpl implements RegisterPresenter,ContactCallBacks {

    RegisterView view;
    @Inject
    ApiManager apiManager;

    @Inject
    PrefManager prefManager;
    @Inject
    Context context;
    public RegisterPresenterImpl(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
    }
    @Override
    public void setView(RegisterView view) {

        this.view = view;
        view.btnRegister();
    }
    @Override
    public void CreateContact(String result, int code) {

    }

    @Override
    public void UpdateContact(String result, int code) {

    }

    @Override
    public void GetContactByID(String result, int code) {

    }

    @Override
    public void GetContactQueryResult(String result, int code) {
        view.hideLoader();
        if (code == 200) {

            try {
                Utility utility = new Utility(prefManager);
                String data[] = utility.jsonPassRegister(result);
                if(data[0]!=null&&data[0].isEmpty()){
                 view.checkData();
                }else {
                    view.requestWaring(context.getString(R.string.internet_error_connection));
                    Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.internet_error_connection)
                            ,"Register");
                }
             } catch (Exception e) {
                e.printStackTrace();
                view.requestWaring(context.getString(R.string.internet_error_connection));
            }
        } else if(code ==0){
            view.requestWaring(context.getString(R.string.internet_error_connection));
            Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
                    ,"Register");

        }

    }



    @Override
    public void RequestTitle() {

    }

    @Override
    public boolean inputData(String email, String pass, String rePass,Context context) {
        if(Utility.emailValidates(email)&&Utility.containData(pass)&&Utility.containData(rePass)&&pass.equals(rePass)) {


//            view.showLoader();
//            apiManager.RegisterContactCallback(this);
//            RNObject rnObject = new RNObject(GeneralTypesEnum.contacts.name());
//
//            String data = "{   }";
//            apiManager.Create(rnObject,data);


            return true;

        }else {
            Log.d("pppp","pppp");
            return false;
        }
    }

    @Override
    public void checkEmail(String email) {
        view.showLoader();
        apiManager.RegisterContactCallback(this);
        apiManager.GetQueryResult(Utility.checkEmail(email), GeneralTypesEnum.contacts.name());
    }
}
