package com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;
import com.mivors.Sjob.project.screens.EditServiceRequest.UpdateServiceRequest;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShowServicesRequestInfoActivity extends BaseActivity implements ShowServicesRequestInfoView {
    @Bind(R.id.titleTV)
    TextView titleTv;

    @Bind(R.id.severity_levelTV)
    TextView severity_levelTV;

    @Bind(R.id.descriptionTV)
    TextView descriptionTV;

    @Bind(R.id.statusTV)
    TextView statusTV;

    @Bind(R.id.dateTV)
    TextView dateTV;

    @Bind(R.id.addressTV)
    TextView addressTV;

    @Bind(R.id.locationTV)
    TextView locationTV;

    @Bind(R.id.edit)
    Button edit;


    ServiceRequest serviceRequest;

    @Inject
    ShowServicesRequestInfoPresenter presenter;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_services_request_info);
        ButterKnife.bind(this);
        ((DaggerApplication)getApplication()).getAppComponent().inject(this);
        serviceRequest = getIntent().getExtras().getParcelable("item");
        bindData(serviceRequest);
        if(!Tokens.getStatus(ShowServicesRequestInfoActivity.this,serviceRequest.getIncidentStatues()).equalsIgnoreCase(getString(R.string.pend))) {
            edit.setVisibility(View.INVISIBLE);
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Tokens.getStatus(ShowServicesRequestInfoActivity.this,serviceRequest.getIncidentStatues()).equalsIgnoreCase(getString(R.string.pend))) {
                    Intent i = new Intent(ShowServicesRequestInfoActivity.this, UpdateServiceRequest.class);
                    Bundle b = new Bundle();
                    b.putParcelable("item", serviceRequest);
                    i.putExtras(b);
                    startActivityForResult(i, Tokens.UPDATE_SERVICE_CODE);
                }
            }
        });

        presenter.setView(this);
        presenter.RequestServicesRequest(serviceRequest.getId());

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(serviceRequest.getTitle());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);   }
    }

    private void bindData(ServiceRequest serviceRequest) {
        titleTv.setText(serviceRequest.getTitle());
        severity_levelTV.setText(Tokens.getSeverity(this,serviceRequest.getSeverityLevel()));
        statusTV.setText(Tokens.getStatus(this,serviceRequest.getIncidentStatues()));
        dateTV.setText(dateAndTimeOperations.getDateForDisplay(serviceRequest.getDate()));
        addressTV.setText(serviceRequest.getStreet());
        if(!serviceRequest.getDescription().equals("null")){
            descriptionTV.setText(serviceRequest.getDescription());
        }
        if(!serviceRequest.getHouseDescription().equals("null")){
            locationTV.setText(serviceRequest.getHouseDescription());
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(serviceRequest.getTitle());
        }

    }

    @Override
    public void showLoader() {
        showProgress();
    }

    @Override
    public void hideLoader() {
        hideProgress();
    }

    @Override
    public void requestServicesRequest(ArrayList<ServiceRequest> serviceRequests) {
        serviceRequest = serviceRequests.get(0);
        bindData(serviceRequest);
    }

    @Override
    public void requestWaring(String message) {
        getWaring(message,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.RequestServicesRequest(serviceRequest.getId());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();

            Utility.UPDATE =11;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(Tokens.UPDATE_SR);
        finish();
        return ;

    }
}
