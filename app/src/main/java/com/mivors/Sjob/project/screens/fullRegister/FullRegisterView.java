package com.mivors.Sjob.project.screens.fullRegister;

/**
 * Created by ahmed radwan on 8/3/2017.
 */

public interface FullRegisterView {
    void showLoader();
    void hideLoader();
    void btnCreateAccount();
    void spinnerView();
    void requestData(String string);
    void requestWaring(String string);

}
