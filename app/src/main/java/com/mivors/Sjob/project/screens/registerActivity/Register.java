package com.mivors.Sjob.project.screens.registerActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.screens.fullRegister.RegisterFullData;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Register extends BaseActivity implements RegisterView {
    @Inject
    RegisterPresenter registerPresenter;

    @Bind(R.id.edEmail)
    EditText edEmail;

    @Bind(R.id.edPass)
    EditText edPass;

    @Bind(R.id.edRePass)
    EditText edRePass;

    @Bind(R.id.btnRegister)
    Button btnRegister;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        ((DaggerApplication) getApplication()).getAppComponent().inject(this);
        registerPresenter.setView(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.register);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);   }
    }

    @Override
    public void showLoader() {
        showProgress();

    }

    @Override
    public void hideLoader() {
        hideProgress();

    }

    @Override
    public void edEmail() {

    }

    @Override
    public void edPassword() {

    }

    @Override
    public void edRePassword() {

    }

    @Override
    public void btnRegister() {
        //// register /////////
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("".equals(edEmail.getText().toString())) {
                    getWaring(getResources().getString(R.string.empty_email), 0);

                } else if ("".equals(edPass.getText().toString())) {
                    getWaring(getResources().getString(R.string.empty_pass), 0);

                } else if ("".equals(edRePass.getText().toString())) {
                    getWaring(getResources().getString(R.string.empty_confirm_pass), 0);

                } else if (!Utility.emailValidates(edEmail.getText().toString())) {
                    getWaring(getResources().getString(R.string.check_email_syntax), 0);

                } else if (!Utility.passValidate(edPass.getText().toString())) {
                    getWaring(getResources().getString(R.string.check_pass), 0);

                } else if (!Utility.passValidate(edRePass.getText().toString())) {
                    getWaring(getResources().getString(R.string.check_re_pass), 0);

                } else if (!Utility.passValidate(edPass.getText().toString(), edRePass.getText().toString())) {
                    getWaring(getResources().getString(R.string.confirm_re_pass), 0);

                } else if(!Utility.checkCharacter(edPass.getText().toString())||
                        !Utility.checkCharacter(edRePass.getText().toString())||
                        !Utility.checkCharacter(edEmail.getText().toString())
                        ) {
                    getWaring(getResources().getString(R.string.check_email_syntax), 0);

                }
                else {
                    registerPresenter.checkEmail(Utility.solveCharacter(edEmail.getText().toString()));

                }


            }
        });
    }

    @Override
    public void requestWaring(String string) {
        getWaring(getResources().getString(R.string.email_repeat), 0);
    }

    @Override
    public void checkData() {
        Intent intent = new Intent(Register.this, RegisterFullData.class);
        intent.putExtra("email", edEmail.getText().toString());
        intent.putExtra("password", edPass.getText().toString());
        startActivityForResult(intent, Tokens.REGISTER_ACTION);
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
