package com.mivors.Sjob.project.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.TypedValue;
import android.widget.ImageView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;


/**
 * Created by Yehia Fathi on 6/16/2016.
 */
public class Tokens {


    public static  final  int REQUEST_CAMERA= 1000;
    public static final  int SELECT_FILE = 1001;
    public static final String CATEGORY_TYPE = "cat_type";
    public static final String CATEGORY_NAME = "cat_name";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_RIGHT_NOW = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_TIME_FORMAT_DISPLAY_FORMAT =  "yyyy-MM-dd hh:mm a";
    public static final int UPDATE_SERVICE_CODE = 2000 ;
    public static final int LOGIN_ACTION = 2001;
    public static final int REGISTER_ACTION =2002 ;
    public static final int LOGIN_SUCCESS = 2003;
    public static final int LOGIN_NAVIGATION = 2004;
    public static final int SUCCESS_REGISTER = 2005;
    public static final int CREATE_SERVICE_REQUEST_SUCCESS = 2006;
    public static final int EDIT_ACCOUNT_SUCCESS = 2007;
    public static final int UPDATE_SR = 2008;


    public static int dpToPx(int dp, Context c) {
        Resources r = c.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }




    public static void send_email(Context c, String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Request Support");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        c.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }



    public static void open_url(Context c, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        c.startActivity(i);
    }

    public static String encodeURIComponent(String s) {
        String result;

        try {
            result = URLEncoder.encode(s, "UTF-8")
                    .replaceAll(" ", "%20");
        } catch (UnsupportedEncodingException e) {
            result = s;
        }

        return result;
    }

    public static SharedPreferences notesPrefs;
    private static  final String PREFKRY ="userinfo";
;    public   static SharedPreferences sharedPreferencesToken(Context context){
        notesPrefs = context.getSharedPreferences(PREFKRY, Context.MODE_PRIVATE);
        return notesPrefs;
    }
    public  static Typeface getFont(Context context){
        Locale current = context.getResources().getConfiguration().locale;
        if(current.getLanguage().equals("ar")){
            String fontPath = "fonts/font.TTF";
            return Typeface.createFromAsset(context.getAssets(), fontPath);
        }else{
            String fontPath = "fonts/english.ttf";
            return Typeface.createFromAsset(context.getAssets(), fontPath);
        }

    }
    public static int  product_position ;





    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(),Base64.NO_WRAP);
    }
    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }


    public static String ImageResult(int requestCode , Intent data, ImageView profile, Context context){
        String Image_bitmap_64 = null;
        if (requestCode == REQUEST_CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            profile.setImageBitmap(thumbnail);
            Image_bitmap_64 = encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG,100);
        } else if (requestCode == SELECT_FILE) {
            Uri selectedImageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            CursorLoader cursorLoader = new CursorLoader(context, selectedImageUri, projection, null, null,
                    null);
            Cursor cursor = cursorLoader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            Bitmap bm;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(selectedImagePath, options);
            profile.setImageBitmap(bm);
            Image_bitmap_64 = encodeToBase64(bm, Bitmap.CompressFormat.JPEG,100);
        }

        return  Image_bitmap_64;
    }

    public  static void selectImage(final Context context) {
        final CharSequence[] items = { "Choose from Library", "Cancel" };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ((Activity)context).startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    ((Activity)context).startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



    public static void setLocale(String lang, Context c) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        c.getApplicationContext().getResources().updateConfiguration(config, null);

    }


    public  static int LOCATION_REQUEST = 1000;

    public  static void change_language(Context context,String lang,PrefManager prefManager) {
            prefManager.update_language(lang);
            Intent i =  ((Activity) context).getIntent();
            ((Activity) context).finish();
            setLocale(lang,context);
            context.startActivity(i);
    }

    public  static void logout(Context context){
        /*Intent intent = new Intent(context, LoginActivity.class);
        PrefManager prefManager = new PrefManager(context);
        prefManager.logout();
        intent.putExtra("finish", true); // if you are checking for this in your other Activities
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();*/
    }

    public  static String getSeverity(Context context,int id){
        if(id == Severities.simple.getValue()){
            return context.getString(R.string.simple);
        }else if(id == Severities.moderate.getValue()){
            return context.getString(R.string.moderate);
        }else{
            return context.getString(R.string.complex);
        }

    }


    public  static int getSevertyID(Context context,int id){
        if(id == Severities.simple.getValue()){
            return 1;
        }else if(id == Severities.moderate.getValue()){
            return 2;
        }else{
            return 3 ;
        }

    }


    public static String getStatus(Context context, int id) {

        if(id == Statues.canceled.getValue()){
            return context.getString(R.string.canceled);
        }else if(id == Statues.pending.getValue()){
            return  context.getString(R.string.pend);
        }else{
            return  context.getString(R.string.completed);
        }
    }

    public static String getCategory(Context context, int id) {

        if(id == Categories.electrical.getValue()){
            return context.getString(R.string.elec);
        }else if(id == Categories.aircondition.getValue()){
            return  context.getString(R.string.airCond);
        }else if(id == Categories.plumbing.getValue()){
            return  context.getString(R.string.plumb);
        }else if(id== Categories.cleaning.getValue()){
            return context.getString(R.string.cleaning);
        }else if(id== Categories.satellite.getValue()){
            return context.getString(R.string.satellite);
        }else if(id== Categories.mobile.getValue()){
            return context.getString(R.string.mobile);
        }

        return "0";
    }



}
