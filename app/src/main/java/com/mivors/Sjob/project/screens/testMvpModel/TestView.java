package com.mivors.Sjob.project.screens.testMvpModel;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

public interface TestView {
     void showLoader();
     void hideLoader();
     void requestTitle(String s);

}
