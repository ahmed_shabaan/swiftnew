package com.mivors.Sjob.project.helpers;

/**
 * Created by Ahmed shaban on 8/2/2017.
 */

public enum Statues {
    pending(1), canceled(102),completed(2);
    private final int value;

    private Statues(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


}
