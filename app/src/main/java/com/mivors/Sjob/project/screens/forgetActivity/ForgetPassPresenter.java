package com.mivors.Sjob.project.screens.forgetActivity;

/**
 * Created by ahmed radwan on 8/1/2017.
 */

public interface ForgetPassPresenter {

    void setView(ForgetPassView view);
    boolean inputData(String email);

}
