package com.mivors.Sjob.project.screens.loginActivity;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Utility;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

import static com.mivors.Sjob.project.screens.showServicesRequestsFragment.ServiceRequestAdapter.context;

/**
 * Created by ahmed radwan on 8/1/2017.
 */

public class LoginPresenterImpl implements LoginPresenter, ContactCallBacks {

    LoginView loginView;
    @Inject
    ApiManager apiManager;

    @Inject
    PrefManager prefManager;
    //    @Inject
//    Utility utility;
    @Inject
    Context context;

    public LoginPresenterImpl(Context context) {
        ((DaggerApplication) context).getAppComponent().inject(this);
    }

    @Override
    public void setView(LoginView loginView) {
        this.loginView = loginView;
        loginView.btnLogin();
    }

    @Override
    public boolean inputData(String email, String pass) {
        if (Utility.emailValidates(email)) {
            loginView.showLoader();
            apiManager.RegisterContactCallback(this);
            apiManager.GetQueryResult(Utility.loginUrl(email), GeneralTypesEnum.contacts.name());
            return true;
        } else {

            return false;
        }
    }

    @Override
    public void showPass() {
        loginView.imgPassView();
    }

    @Override
    public void action() {

//        loginView.showLoader();
//        apiManager.RegisterContactCallback(this);
//        apiManager.GetQueryResult("Select Contacts.ID,Contacts.CustomFields.c.password FROM Contacts" );
    }

    @Override
    public void forgetAction() {
        loginView.txtForgetPass();
    }

    @Override
    public void checkRemember() {
        loginView.chRemember();
    }

    @Override
    public void checkLogin() {
        loginView.chKeepMeLogin();
    }

    @Override
    public void CreateContact(String result, int code) {

    }

    @Override
    public void UpdateContact(String result, int code) {

    }

    @Override
    public void GetContactByID(String result, int code) {

    }

    @Override
    public void GetContactQueryResult(String result, int code) {

        String data = result;
        loginView.hideLoader();
        if (code == 200) {

            try {
                Utility utility = new Utility(prefManager);
                String pass = utility.jsonPass(result);
                //  prefManager.setLogin();

                loginView.requestLogin(pass);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (code == 0) {
            loginView.requestWaring(context.getString(R.string.internet_error_connection));
            Utility.setCrashlyticsLog(code, "", context.getResources().getString(R.string.internet_error_connection)
                    , "Register");
        } else {
            loginView.requestWaring(context.getString(R.string.sms_send_error));
            Utility.setCrashlyticsLog(code, result, context.getResources().getString(R.string.sms_send_error)
                    , "Register");

        }
    }


}
