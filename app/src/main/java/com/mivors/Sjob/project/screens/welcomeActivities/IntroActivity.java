package com.mivors.Sjob.project.screens.welcomeActivities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.widget.Button;

import com.github.paolorotolo.appintro.AppIntro;
import com.mivors.Sjob.R;
import com.mivors.Sjob.project.helpers.Languages;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.screens.homeActivity.home;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.Tutorial_five_fragment;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.Tutorial_four_fragment;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.Tutorial_one_fragment;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.Tutorial_three_fragment;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.Tutorial_two_fragment;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.fourthFragmentLoaded;
import com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments.lastFragmentLoaded;

/**
 * Created by Ahmed shaban on 8/6/2017.
 */

public class IntroActivity extends AppIntro implements lastFragmentLoaded, fourthFragmentLoaded {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        addSlide(new Tutorial_one_fragment());
        addSlide(new Tutorial_two_fragment());
        addSlide(new Tutorial_three_fragment());
        addSlide(new Tutorial_four_fragment());
        addSlide(new Tutorial_five_fragment());
        showDoneButton(false);
        showSkipButton(false);
        setZoomAnimation();
        setDoneText(getResources().getString(R.string.goButton));
        setSkipText(getResources().getString(R.string.skipbutton));
        String lang =  getResources().getConfiguration().locale.getDisplayLanguage();

        PrefManager prefManager = new PrefManager(this);
        if (prefManager.get_lang().equals(Languages.ARABIC.getValue())) {
            nextButton.setRotation(180);
        }
        nextButton.setVisibility(Button.INVISIBLE);

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        try {
            Intent i = new Intent (this, home.class);
            finish();
            startActivity(i);
        } catch ( ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        Intent i = new Intent (this, home.class);
        finish();
        startActivity(i);
        super.onDonePressed(currentFragment);
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }





    public void setSkipButtonEnabled(boolean skipButtonEnabled) {
        this.skipButtonEnabled = skipButtonEnabled;
    }

    @Override
    public void getValueFromFragmentFive(Boolean loaded) {

    }

    @Override
    public void getValueFromFragmentFour(Boolean loaded) {

    }
}
