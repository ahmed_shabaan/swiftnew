package com.mivors.Sjob.project.screens.homeFragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiClient.ApiInterface;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Tokens;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

///**
// * A simple {@link Fragment} subclass.
// * Use the {@link HomeFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements HomeView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Inject
    HomePresenter homePresenter;

    @Bind(R.id.plumbingImageView)
    ImageView plumbingImageView;

    @Bind(R.id.airCondImageView)
    ImageView airCondImageView;


    @Bind(R.id.electricImageView)
    ImageView electricImageView;

    @Bind(R.id.cleaning)
    ImageView cleaning;


    @Bind(R.id.mobile)
    ImageView mobile;


    @Bind(R.id.satellite)
    ImageView satellite;



    @Inject
    ApiInterface apiInterface;



    public HomeFragment() {
        // Required empty public constructor
    }

//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment AboutUsFragment.
//     */
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment AboutUsFragment.

    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
//        HomeFragment fragment = new HomeFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((DaggerApplication)getActivity().getApplication()).getAppComponent().inject(this);
         setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((DaggerApplication)getActivity().getApplication()).getAppComponent().inject(this);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!=null){
            actionBar.setTitle(R.string.menus);

        }
        homePresenter.setView(this);
        setImagesLnag();
        plumbingImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.RequestPlumbing();
            }
        });

        airCondImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.RequestAirConditioning();
            }
        });

        electricImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.RequestElectrical();
            }
        });

        satellite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.RequestSatelite();
            }
        });

        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.RequestMobile();
            }
        });


        cleaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.RequestCleaning();
            }
        });
        return view;
    }

    @Override
    public void launchAirConditioning(Intent intent) {
      startActivityForResult(intent, Tokens.LOGIN_NAVIGATION);
    }

    @Override
    public void launchPlumbing(Intent intent) {
        startActivityForResult(intent, Tokens.LOGIN_NAVIGATION);
    }

    @Override
    public void launchElectrical(Intent intent) {
      startActivityForResult(intent, Tokens.LOGIN_NAVIGATION);

    }

    @Override
    public void launchSatellite(Intent i) {
        startActivityForResult(i, Tokens.LOGIN_NAVIGATION);
    }

    @Override
    public void launchMobile(Intent i) {
        startActivityForResult(i, Tokens.LOGIN_NAVIGATION);
    }

    @Override
    public void launchCleaning(Intent i) {
        startActivityForResult(i, Tokens.LOGIN_NAVIGATION);
    }

    void setImagesLnag()
    {
        Locale current = getResources().getConfiguration().locale;
        if(current.equals(Locale.ENGLISH))
        {
            airCondImageView.setBackgroundResource(R.mipmap.ac);
            plumbingImageView.setBackgroundResource(R.mipmap.pl);
            electricImageView.setBackgroundResource(R.mipmap.elc);
            satellite.setBackgroundResource(R.mipmap.satellite_en);
            mobile.setBackgroundResource(R.mipmap.mobile_en);
            cleaning.setBackgroundResource(R.mipmap.cleaning_en);

        }
        else
        {

            airCondImageView.setBackgroundResource(R.mipmap.ac_ar);
            plumbingImageView.setBackgroundResource(R.mipmap.pl_ar);
            electricImageView.setBackgroundResource(R.mipmap.elc_ar);
            satellite.setBackgroundResource(R.mipmap.satellite_ar);
            mobile.setBackgroundResource(R.mipmap.mobile_ar);
            cleaning.setBackgroundResource(R.mipmap.cleaning_ar);


        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
