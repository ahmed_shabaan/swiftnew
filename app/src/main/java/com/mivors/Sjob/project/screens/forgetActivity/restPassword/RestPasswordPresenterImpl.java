package com.mivors.Sjob.project.screens.forgetActivity.restPassword;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.Contact;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.RightNow.RNObject;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

/**
 * Created by Ahmed shaban on 8/23/2017.
 */

public class RestPasswordPresenterImpl implements RestPasswordPresenter , ContactCallBacks {
    RestPasswordView  view;
    @Inject
    ApiManager apiManager;
     String verificationCode;
     String accountID;
    Context context;

    @Override
    public void setView(RestPasswordView view) {
        this.view = view;
    }

    public RestPasswordPresenterImpl(Context context) {
        ((DaggerApplication)context).getAppComponent().inject(this);
        this.context = context;

    }

    @Override
    public Void sendCode(String email) {
        if(view.getInsertedCode().isEmpty()){
            view.requestWaring(R.string.promot_code);
        }else{
            String query ="select Contacts.CustomFields.c.reset_pass_code , Contacts.ID,Contacts.CustomFields.c.password  from Contacts   " +
                    "   where   Contacts" +
                    ".Emails.EmailList.Address = '"+email+"' ";
            apiManager.RegisterContactCallback(this);
            apiManager.GetQueryResult(query, GeneralTypesEnum.contacts.name());
            view.showLoader();
        }

        return  null;
    }

    @Override
    public Void updatedPassword(String password,String confirmPassword) {
        if(password.length() < 5){
            view.requestWaring(R.string.password_promot);
        }else if(confirmPassword.isEmpty()){
            view.requestWaring(R.string.empty_confirm_pass);

        }
        else if(!password.equals(confirmPassword)){
             view.requestWaring(R.string.password_not_matched);
        }else{

            String body ="{\"customFields\":" +
                    " {\"c\": {\"reset_pass_code\": \"\",\"password\":\""+password+"\"}}}";
            RNObject rnObject = new RNObject(GeneralTypesEnum.contacts.name());
            rnObject.setId(Integer.parseInt(accountID));
             view.showLoader();
                apiManager.Update(rnObject,body);

        }
        return null;
    }

    @Override
    public void CreateContact(String result, int code) {

    }

    @Override
    public void UpdateContact(String result, int code) {
        view.hideLoader();
        if(code==200||code==201 || code==202) {
            view.getSuccess(R.string.password_updated_successfully);
        }else if(code==408){
            view.requestWaring(R.string.internet_error_connection);
        }else{
            view.requestWaring(R.string.tryAgain);
        }

    }

    @Override
    public void GetContactByID(String result, int code) {

    }

    @Override
    public void GetContactQueryResult(String result, int code) {
        view.hideLoader();
        if(code==200||code==201) {
            parsQueryResult(result);
            if(verificationCode.equals(view.getInsertedCode())){
                view.enableViews();
            }else{
                view.requestWaring(R.string.wrong_code);
                Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.wrong_code)
                        ,"RestPassword");

            }
        }else if(code==408){
            view.requestWaring(R.string.sms_send_error);
            Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"RestPassword");

        }else if(code == 0){
            view.requestWaring(R.string.internet_error_connection);
            Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
                    ,"RestPassword");
        }else
        {
            view.requestWaring(R.string.sms_send_error);
         Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"RestPassword");


        }
    }



    public  void  parsQueryResult (String  Json)
    {

        try {
            JSONObject response = new JSONObject(Json);
            JSONArray Items = response.getJSONArray("items");
            JSONObject item = Items.getJSONObject(0);
            JSONArray rows  = item.getJSONArray("rows");
            JSONArray  info = rows.getJSONArray(0);
            verificationCode =info.getString(0);
            accountID =info.getString(1);




        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
