package com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity;

import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;

import java.util.ArrayList;

/**
 * Created by Ahmed shaban on 8/9/2017.
 */

public interface ShowServicesRequestInfoPresenter {
    void setView(ShowServicesRequestInfoView view);
    void RequestServicesRequest(String id);
}
