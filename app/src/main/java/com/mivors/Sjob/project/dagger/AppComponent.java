package com.mivors.Sjob.project.dagger;

import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.baseClasses.BaseFragment;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.screens.EditServiceRequest.UpdateServiceRequest;
import com.mivors.Sjob.project.screens.EditServiceRequest.UpdateServiceRequestPresenterImp;
import com.mivors.Sjob.project.screens.MainActivity;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoActivity;
import com.mivors.Sjob.project.screens.ShowServicesRequestInfoActivity.ShowServicesRequestInfoImpl;
import com.mivors.Sjob.project.screens.editAccount.EditAccount;
import com.mivors.Sjob.project.screens.editAccount.EditAccountPresenterImpl;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassPresenter;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassPresenterImpl;
import com.mivors.Sjob.project.screens.forgetActivity.ForgetPassword;
import com.mivors.Sjob.project.screens.about_usFragment.AboutUsFragment;
import com.mivors.Sjob.project.screens.about_usFragment.AboutUsPresenterImp;
import com.mivors.Sjob.project.screens.createServiceRequestActivity.CreateServiceRequest;
import com.mivors.Sjob.project.screens.createServiceRequestActivity.CreateServiceRequestPresenterImp;
import com.mivors.Sjob.project.screens.forgetActivity.restPassword.RestPass_Verify;
import com.mivors.Sjob.project.screens.forgetActivity.restPassword.RestPasswordPresenterImpl;
import com.mivors.Sjob.project.screens.fullRegister.FullRegisterPresenterImpl;
import com.mivors.Sjob.project.screens.fullRegister.RegisterFullData;
import com.mivors.Sjob.project.screens.homeActivity.home;
import com.mivors.Sjob.project.screens.homeFragment.HomeFragment;
import com.mivors.Sjob.project.screens.homeFragment.HomePresenterImp;
import com.mivors.Sjob.project.screens.loginActivity.Login;
import com.mivors.Sjob.project.screens.loginActivity.LoginPresenter;
import com.mivors.Sjob.project.screens.loginActivity.LoginPresenterImpl;
import com.mivors.Sjob.project.screens.registerActivity.Register;
import com.mivors.Sjob.project.screens.registerActivity.RegisterPresenter;
import com.mivors.Sjob.project.screens.registerActivity.RegisterPresenterImpl;
import com.mivors.Sjob.project.screens.settings.SettingsFragment;
import com.mivors.Sjob.project.screens.showServicesRequestsFragment.ServicesRequestsPresenterImpl;
import com.mivors.Sjob.project.screens.showServicesRequestsFragment.ShowServicesRequestsFragment;
import com.mivors.Sjob.project.screens.testMvpModel.TestMvpActivity;
import com.mivors.Sjob.project.screens.testMvpModel.TestPresenterImpl;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class,PresenterModule.class})
public interface AppComponent {
    void inject(DaggerApplication daggerApplication);
    void inject(ApiManager apiManager);
    void inject(MainActivity mainActivity);
    void inject(TestPresenterImpl testPresenter);
    void inject(TestMvpActivity testMvpActivity);
    void inject(BaseActivity baseActivity);
    void inject(HomePresenterImp homePresenterImp );
    void inject(HomeFragment homeFragment);
    void inject(BaseFragment baseFragment);
    void inject(RegisterPresenterImpl registerPresenter);
    void inject(Register register);
    void inject(LoginPresenterImpl loginPresenter);
    void inject(Login login);
    void inject(ForgetPassword forgetPassword);
    void inject(ForgetPassPresenterImpl forgetPassPresenter);
    void inject(CreateServiceRequestPresenterImp createServiceRequestPresenterImp );
    void inject(CreateServiceRequest createServiceRequest );
    void inject(AboutUsFragment aboutUsFragment );
    void inject(AboutUsPresenterImp aboutUsPresenterImp );
    void inject(RegisterFullData registerFullData);
    void inject(FullRegisterPresenterImpl forgetPassPresenter);
    void inject(ServicesRequestsPresenterImpl servicesRequestsPresenter );
    void inject(ShowServicesRequestsFragment showServicesRequestsFragment );
    void inject(EditAccount editAccount);
    void inject(EditAccountPresenterImpl editAccountPresenter);
    void inject(UpdateServiceRequestPresenterImp updateServicesRequestsPresenter );
    void inject(UpdateServiceRequest updateServiceRequest );
    void inject(SettingsFragment settingsFragment );
    void inject(ShowServicesRequestInfoImpl target );
    void inject(ShowServicesRequestInfoActivity target);
    void inject(RestPasswordPresenterImpl restPasswordPresenter);
    void inject(RestPass_Verify restPass_verify);
//    void inject(home home);








}
