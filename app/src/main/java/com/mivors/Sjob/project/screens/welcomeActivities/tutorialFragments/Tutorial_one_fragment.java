package com.mivors.Sjob.project.screens.welcomeActivities.tutorialFragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;
import com.mivors.Sjob.R;
import com.mivors.Sjob.project.screens.homeActivity.home;
import com.mivors.Sjob.project.screens.welcomeActivities.LanguageSelector;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tutorial_one_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tutorial_one_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tutorial_one_fragment extends Fragment implements ISlideBackgroundColorHolder {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView start , back , startar ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Tutorial_one_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tutorial_one_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Tutorial_one_fragment newInstance(String param1, String param2) {
        Tutorial_one_fragment fragment = new Tutorial_one_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View  rootView = inflater.inflate(R.layout.tutorial_one_fragment, container, false);
        start = (TextView) rootView.findViewById(R.id.textView7);
        back = (TextView) rootView.findViewById(R.id.textView8);
        startar = (TextView) rootView.findViewById(R.id.textView9);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InHome = new Intent(Tutorial_one_fragment.this.getActivity(), home.class);
                Tutorial_one_fragment.this.getActivity().startActivity(InHome);
                Tutorial_one_fragment.this.getActivity().finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InHome = new Intent(Tutorial_one_fragment.this.getActivity(), LanguageSelector.class);
                Tutorial_one_fragment.this.getActivity().startActivity(InHome);
                Tutorial_one_fragment.this.getActivity().finish();
            }
        });

        startar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InHome = new Intent(Tutorial_one_fragment.this.getActivity(), home.class);
                Tutorial_one_fragment.this.getActivity().startActivity(InHome);
                Tutorial_one_fragment.this.getActivity().finish();
            }
        });



        String lang =  getResources().getConfiguration().locale.getDisplayLanguage();
        if (lang.equals("English")) {
            System.out.println("enter here667");
            //  parentView.setBackgroundResource(R.mipmap.about);

            start.setVisibility(View.INVISIBLE);
            startar.setVisibility(View.VISIBLE);

        }
        else if (lang.equals("العربية")) {
            System.out.println("enter here668");

            //  parentView.setBackgroundResource(R.mipmap.about_ar);
            start.setVisibility(View.VISIBLE);
            startar.setVisibility(View.INVISIBLE);
        }
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public int getDefaultBackgroundColor() {
        return Color.parseColor("#fff307");
    }

    @Override
    public void setBackgroundColor(@ColorInt int backgroundColor) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
