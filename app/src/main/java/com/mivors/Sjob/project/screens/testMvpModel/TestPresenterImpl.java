package com.mivors.Sjob.project.screens.testMvpModel;

import android.content.Context;
import android.widget.TextView;

import com.mivors.Sjob.RightNow.ApiClient.ApiInterface;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.RNClasses.Incident.IncidentCallBacks;
import com.mivors.Sjob.project.helpers.DaggerApplication;

import javax.inject.Inject;

/**
 * Created by Ahmed shaban on 7/30/2017.
 */

public class TestPresenterImpl implements testPresenter ,IncidentCallBacks {
    TestView testView;

    @Inject
    ApiManager apiManager;

    public TestPresenterImpl(Context context) {
        ((DaggerApplication) context).getAppComponent().inject(this);
    }
    @Override
    public void setView(TestView view) {
        this.testView = view;
    }

    @Override
    public void RequestTitle() {
        testView.showLoader();
        apiManager.RegisterIncidentCallback(this);
       // apiManager.GetQueryResult("SELECT id FROM Incidents LIMIT 25");

    }


    @Override
    public void CreateIncident(String result, int code) {

    }

    @Override
    public void UpdateIncident(String result, int code) {

    }

    @Override
    public void GetIncidentByID(String result, int code) {

    }

    @Override
    public void GetIncidentQueryResult(String result, int code) {
        testView.hideLoader();
        testView.requestTitle(result);
    }
}
