package com.mivors.Sjob.project.screens.homeActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.callBacks.LocationChangeResult;
import com.mivors.Sjob.project.helpers.Languages;
import com.mivors.Sjob.project.helpers.PrefManager;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.User;
import com.mivors.Sjob.project.screens.about_usFragment.AboutUsFragment;
import com.mivors.Sjob.project.screens.editAccount.EditAccount;
import com.mivors.Sjob.project.screens.homeFragment.HomeFragment;
import com.mivors.Sjob.project.screens.loginActivity.Login;
import com.mivors.Sjob.project.screens.registerActivity.Register;
import com.mivors.Sjob.project.screens.settings.SettingsFragment;
import com.mivors.Sjob.project.screens.showServicesRequestsFragment.ShowServicesRequestsFragment;

import javax.inject.Inject;

public class home extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,LocationChangeResult {

    private FragmentManager manager;
    private FragmentTransaction transaction;
    MenuItem login, register, logout, myAccount, myServiceRequests ;
    String Naviagtion = "";
    boolean skipMethod = false;
    DrawerLayout drawer;
    Fragment fragment=null;
    TextView name;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        assert getSupportActionBar() != null;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.menus));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


          drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        Drawable menuIcon = ContextCompat.getDrawable(getApplicationContext(), R.drawable.menu);
        toolbar.setNavigationIcon(menuIcon);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header=navigationView.getHeaderView(0);
         name = (TextView)header.findViewById(R.id.parTitleTextView);


        navigationView.setNavigationItemSelectedListener(this);
        myAccount  = navigationView.getMenu().findItem(R.id.myAccount);
        myServiceRequests  = navigationView.getMenu().findItem(R.id.MyIncidents);
        myAccount = navigationView.getMenu().findItem(R.id.myAccount);
        myServiceRequests = navigationView.getMenu().findItem(R.id.MyIncidents);

        try {
            Naviagtion = getIntent().getExtras().getString("Navigation");
            if (Naviagtion.equals("")) {
                Fragment fragment = new ShowServicesRequestsFragment();
                openFragment(fragment, "ShowServicesRequestsFragment");
            } else {
                Fragment fragment = new HomeFragment();
                openFragment(fragment, "home");
                skipMethod = true;
            }
        }
        catch (Exception e)
        {
            Fragment fragment = new HomeFragment();
            openFragment(fragment, "home");
            skipMethod = true;
        }

        skipMethod = true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        login = menu.getItem(0);
        register = menu.getItem(1);
        logout = menu.getItem(2);
        LoginStateChange();
        return true;
    }

    private void LoginStateChange() {

        if (prefManager.check_login()|| Utility.LOGIN_ONCE ==1) {
            if (login != null)
                login.setVisible(false);
            if (register != null)
                register.setVisible(false);
            if (logout != null)
                logout.setVisible(true);
            if (myAccount != null)
                myAccount.setVisible(true);
            if (myServiceRequests != null)
                myServiceRequests.setVisible(true);

                name.setText(prefManager.getFirstName()+ "  "+ prefManager.getLastName());

        } else if (prefManager.check_login()&& prefManager.getKeepMeLogin()) {
            if (login != null)
                login.setVisible(false);
            if (register != null)
                register.setVisible(false);
            if (logout != null)
                logout.setVisible(true);
            if (myAccount != null)
                myAccount.setVisible(true);
            if (myServiceRequests != null)
                myServiceRequests.setVisible(true);
        }

        else {
            if (login != null)
                login.setVisible(true);
            if (register != null)
                register.setVisible(true);
            if (logout != null)
                logout.setVisible(false);
            if (myAccount != null)
                myAccount.setVisible(false);
            if (myServiceRequests != null)
                myServiceRequests.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            prefManager.logout();
            prefManager.setLogin(false);

            prefManager.set_data(new User());
            Utility.LOGIN_ONCE = 0;
            finish();
            Intent i = new Intent(this, home.class);
            startActivity(i);
             return true;
        } else if (id == R.id.login) {

            Intent i = new Intent(this, Login.class);
            startActivityForResult(i, Tokens.LOGIN_ACTION);
            return true;
        } else if (id == R.id.createAccount) {
            Intent i = new Intent(this, Register.class);
            startActivityForResult(i, Tokens.REGISTER_ACTION);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.createIcident:
                fragment = new HomeFragment();
                openFragment(fragment, "home");
                break;
            case R.id.MyIncidents:
                fragment = new ShowServicesRequestsFragment();
                openFragment(fragment, "ShowServicesRequestsFragment");
                break;
            case R.id.myAccount:
                fragment = new EditAccount();
                openFragment(fragment, "editAccount");
                break;
            case R.id.settings:
                fragment = new SettingsFragment();
                openFragment(fragment, "Settings");
                break;
            case R.id.aboutUs:
                fragment = new AboutUsFragment();
                openFragment(fragment, "AboutUs");
                break;
             case R.id.ar:
                Tokens.change_language(this, Languages.ARABIC.getValue(), prefManager);
                break;
            case R.id.english:
                Tokens.change_language(this, Languages.ENGLISH.getValue(), prefManager);
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void openFragment(Fragment fragment, String tag) {
        manager = getSupportFragmentManager();
        if(manager == null)
        manager  =  getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        if(tag.equals("home")){
            transaction.replace(R.id.content_home, fragment, tag).commit();
        }else{
            Fragment currentFrag = manager.findFragmentById(R.id.content_home);
            if (currentFrag != null && currentFrag.getClass().equals(fragment.getClass())) {

            } else {
                transaction.replace(R.id.content_home, fragment).addToBackStack(null).commit();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LoginStateChange();


    }

    @Override
    public void getAddressLocation(String s ,String lat,String lng) {
     if(fragment instanceof EditAccount){
         ((EditAccount) fragment).getAddressLocation(s,lat,lng);
     }

    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        if(manager.getBackStackEntryCount() > 0){
                manager.popBackStackImmediate();
        }else{
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.please_click_back, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }


    }
}
