package com.mivors.Sjob.project.screens.loginActivity;

/**
 * Created by ahmed radwan on 8/1/2017.
 */

public interface LoginPresenter {

    void setView(LoginView loginView);
    boolean inputData(String email,String pass);
    void showPass();
    void action();
    void forgetAction();
    void checkRemember();
    void checkLogin();

}
