package com.mivors.Sjob.project.screens.welcomeActivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mivors.Sjob.R;
import com.mivors.Sjob.project.baseClasses.BaseActivity;
import com.mivors.Sjob.project.helpers.Languages;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.screens.homeActivity.home;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LanguageSelector extends BaseActivity {
    @Bind(R.id.setEngLng)
    Button english;

    @Bind(R.id.setArLng)
    Button arabic;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selector);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefManager.update_language(Languages.ENGLISH.getValue());
                Tokens.setLocale(Languages.ENGLISH.getValue(),LanguageSelector.this);
                LaunchActivity();


            }
        });

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefManager.update_language(Languages.ARABIC.getValue());
                Tokens.setLocale(Languages.ARABIC.getValue(),LanguageSelector.this);
                LaunchActivity();

            }
        });

    }


    private void LaunchActivity(){

        if(prefManager.check_login()||Utility.LOGIN_ONCE==1){

            startActivity(new Intent(LanguageSelector.this, home.class) );
        }else{
            startActivity(new Intent(LanguageSelector.this, IntroActivity.class) );
        }

        finish();
    }
}
