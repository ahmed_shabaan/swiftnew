package com.mivors.Sjob.project.screens.showServicesRequestsFragment;

import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;

import java.util.ArrayList;

/**
 * Created by Ahmed shaban on 8/2/2017.
 */

public interface ServiceRequestView {
     void showLoader();
     void hideLoader();
     void showServiceRequests(ArrayList<ServiceRequest> serviceRequests);

    void requestWaring(String string);

    void updateList(ArrayList<ServiceRequest> temp);
}
