package com.mivors.Sjob.project.screens.showServicesRequestsFragment;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.core.CrashlyticsCore;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Incident.IncidentCallBacks;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.DateAndTimeOperations;
import com.mivors.Sjob.project.helpers.Statues;
import com.mivors.Sjob.project.helpers.Tokens;
import com.mivors.Sjob.project.helpers.Utility;
import com.mivors.Sjob.project.model.serviceRequest.ServiceRequest;
import com.mivors.Sjob.project.model.serviceRequest.ServicesRequestParser;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.Crash;

/**
 * Created by Ahmed shaban on 8/2/2017.
 */

public class ServicesRequestsPresenterImpl implements ServicesRequestsPresenter,IncidentCallBacks{
    ServiceRequestView view;
    @Inject
    ApiManager apiManager;

    ArrayList<ServiceRequest> mainList ;
    ArrayList<ServiceRequest> filterList ;

    @Inject
    Context context;
    @Override
    public void setView(ServiceRequestView view) {
        this.view = view;
    }

    public ServicesRequestsPresenterImpl(Context context){
        ((DaggerApplication)context).getAppComponent().inject(this);
         mainList  = new ArrayList<>();
         filterList = new ArrayList<>();

    }

    @Override
    public void getServiceRequests(int contactId) {
        view.showLoader();
        apiManager.RegisterIncidentCallback(this);
        String query = "SELECT C.CustomFields.c.c_product , C.ID , C.CustomFields.c.severity_level , C.CustomFields.c.description , Subject , C.CustomFields.c.gpslocation  ,C.CustomFields.c.house_description , " +
                "C.CustomFields.c.date ,C.CustomFields.TOA.city , C.CustomFields.TOA.street, Incidents.StatusWithType.Status " +
                ", C.CustomFields.c.technician_name , C.CustomFields.c.technician_rate ,  C.CustomFields.c.service_rate , C.ReferenceNumber" +
                " , C.CustomFields.c.cost , C.CustomFields.c.complain_title ,"
                + "C.CustomFields.c.customer_feedback , C.CustomFields.c.complain_start_time ," +
                "C.CustomFields.c.complain_closed_time ,C.CustomFields.c.complaint_status,C.CustomFields.TOA.coordinate_x,C.CustomFields.TOA.coordinate_y , C.CustomFields.c.sr_payment_method "+
                " From Incidents  C  WHERE PrimaryContact.Contact.ID="+contactId+"   order by C.CustomFields.c.date desc ";

        apiManager.GetQueryResult(query, GeneralTypesEnum.incidents.name());
    }

    @Override
    public void CreateIncident(String result, int code) {

    }

    @Override
    public void UpdateIncident(String result, int code) {

    }

    @Override
    public void GetIncidentByID(String result, int code) {

    }

    @Override
    public void GetIncidentQueryResult(String result, int code) {
        view.hideLoader();
        Log.d("ppp555",result+"   "+code);
        if(code==200){
            try {
                mainList.addAll(sortServiceRequest(ServicesRequestParser.ParseServiceRequests(result)));
                view.showServiceRequests(mainList);

            } catch (JSONException e) {
                e.printStackTrace();
                Crashlytics.logException(e);
            }
        }else if (code ==0){
            view.requestWaring(context.getString(R.string.internet_error_connection));
            Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
            ,"ServicesRequests");
        }

        else{
            view.requestWaring(context.getString(R.string.sms_send_error));
               Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_error)
                    ,"ServicesRequests");
        }
    }



    public ArrayList<ServiceRequest> sortServiceRequest(ArrayList<ServiceRequest> mySRsList){

        if(mySRsList != null && mySRsList.size()==0){
            Toast.makeText(context, R.string.no_service_requests, Toast.LENGTH_SHORT).show();
        }
        ArrayList<ServiceRequest> temp = new   ArrayList<>();

        for (int i = 0 ; i < 3 ; i++){
            for (int j=0 ; j < mySRsList.size() ; j++ ){
                if(i==0 && mySRsList.get(j).getIncidentStatues() == Statues.pending.getValue()){
                    temp.add(mySRsList.get(j));
                }else if(i==1 && mySRsList.get(j).getIncidentStatues() == Statues.completed.getValue()){
                    temp.add(mySRsList.get(j));
                }else if(i==2 && mySRsList.get(j).getIncidentStatues() == Statues.canceled.getValue()){
                    temp.add(mySRsList.get(j));
                }
            }
        }

        mySRsList.clear();
        mySRsList = temp;
        mainList  =  new ArrayList<>();
        mainList.addAll(mySRsList);

        return mySRsList;
    }

    @Override
    public void apply_filter(String from,String to, String category,String status){
        ArrayList<ServiceRequest>  temp  = new ArrayList<ServiceRequest>();
        filterList.clear();
        filterList.addAll(mainList) ;
        DateAndTimeOperations dateAndTimeOperations = new DateAndTimeOperations();

        if(!status.equals(context.getString(R.string.all))  && !category.equals(context.getString(R.string.all))   ){
            for (int i=0 ;i < filterList.size();i++){

                String[] srDate = dateAndTimeOperations.getCurrentDataWithTimeZoneForCompare(filterList.get(i).getDate()).split("T");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date DateFrom=null,DateTo=null,DateSr=null;
                try {
                    DateFrom = df.parse(from);
                    DateTo = df.parse(to);
                    DateSr = df.parse(srDate[0]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if( Tokens.getStatus(context,filterList.get(i).getIncidentStatues()).equals(status) && Tokens.getCategory(context,filterList.get(i).getCategory()).equals(category) && (DateSr.after(DateFrom) && DateSr.before(DateTo)) ){
                    temp.add(filterList.get(i));
                }

            }
        }else if(!status.equals(context.getString(R.string.all))){
            for (int i=0 ;i < filterList.size();i++){
                String[] srDate = dateAndTimeOperations.getCurrentDataWithTimeZoneForCompare(filterList.get(i).getDate()).split("T");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date DateFrom=null,DateTo=null,DateSr=null;
                try {
                    DateFrom = df.parse(from);
                    DateTo = df.parse(to);
                    DateSr = df.parse(srDate[0]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(Tokens.getStatus(context,filterList.get(i).getIncidentStatues()).equals(status) && (DateSr.after(DateFrom) && DateSr.before(DateTo))  ){
                    temp.add(filterList.get(i));
                }

            }
        }else if(!category.equals(context.getString(R.string.all))){
            for (int i=0 ;i < filterList.size();i++){
                String[] srDate = dateAndTimeOperations.getCurrentDataWithTimeZoneForCompare(filterList.get(i).getDate()).split("T");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date DateFrom=null,DateTo=null,DateSr=null;
                try {
                    DateFrom = df.parse(from);
                    DateTo = df.parse(to);
                    DateSr = df.parse(srDate[0]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(Tokens.getCategory(context,filterList.get(i).getCategory()).equals(category) && (DateSr.after(DateFrom) && DateSr.before(DateTo)) ){
                    temp.add(filterList.get(i));
                }

            }
        }else{
            for (int i=0 ;i < filterList.size();i++){
                String[] srDate =dateAndTimeOperations.getCurrentDataWithTimeZoneForCompare(filterList.get(i).getDate()).split("T");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date DateFrom=null,DateTo=null,DateSr=null;
                try {
                    DateFrom = df.parse(from);
                    DateTo = df.parse(to);
                    DateSr = df.parse(srDate[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if(DateSr !=null &&  DateSr.after(DateFrom) && DateSr.before(DateTo)||
                        DateSr.equals(DateFrom) && DateSr.equals(DateTo)){
                    temp.add(filterList.get(i));
                }

            }
        }


       // clear.setVisible(true);
        filterList.clear();
        filterList.addAll(temp);
        view.updateList(temp);


    }

    @Override
    public void restList() {
        view.updateList(mainList);
    }

}
