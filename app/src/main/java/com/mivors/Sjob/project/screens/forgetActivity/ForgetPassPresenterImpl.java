package com.mivors.Sjob.project.screens.forgetActivity;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.mivors.Sjob.R;
import com.mivors.Sjob.RightNow.ApiManager.ApiManager;
import com.mivors.Sjob.RightNow.GeneralTypesEnum;
import com.mivors.Sjob.RightNow.RNClasses.Contact.ContactCallBacks;
import com.mivors.Sjob.RightNow.RNObject;
import com.mivors.Sjob.project.helpers.DaggerApplication;
import com.mivors.Sjob.project.helpers.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import static com.mivors.Sjob.project.screens.showServicesRequestsFragment.ServiceRequestAdapter.context;

/**
 * Created by ahmed radwan on 8/1/2017.
 */

public class ForgetPassPresenterImpl implements ForgetPassPresenter, ContactCallBacks {

    ForgetPassView view;
    @Inject
    ApiManager apiManager;
    Context context;

    public ForgetPassPresenterImpl(Context context) {
        ((DaggerApplication) context).getAppComponent().inject(this);
        this.context = context;
    }

    @Override
    public void setView(ForgetPassView view) {
        this.view = view;
        view.btnForgetPass();
    }

    @Override
    public boolean inputData(String email) {
        if (Utility.emailValidates(email)) {
            view.showLoader();
            apiManager.RegisterContactCallback(this);
            RNObject rnObject = new RNObject(GeneralTypesEnum.contacts.name());

            String data = "";
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("contact_mail", email);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data = jsonObject.toString();
            apiManager.forgetPass(rnObject, email);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void CreateContact(String result, int code) {
        try {
            view.hideLoader();
            Log.d("pppfff", code + "  " + result);
            if (result.equals("1")) {
                view.requestTitle(context.getResources().getString(R.string.sms_send_success));
            } else if(result.equals("15")){
                view.requestTitle(context.getResources().getString(R.string.sms_send_wrong_number));
                Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.sms_send_wrong_number)
                        ,"ForgetPass");
            }else if(code==0){
                view.requestTitle(context.getString(R.string.internet_error_connection));
                Utility.setCrashlyticsLog(code,"",context.getResources().getString(R.string.internet_error_connection)
                        ,"ForgetPass");
            } else
                {
                view.requestTitle(context.getString(R.string.wrong_code));
                    Utility.setCrashlyticsLog(code,result,context.getResources().getString(R.string.wrong_code)
                            ,"ForgetPass");
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void UpdateContact(String result, int code) {

    }

    @Override
    public void GetContactByID(String result, int code) {

    }

    @Override
    public void GetContactQueryResult(String result, int code) {

    }


}
