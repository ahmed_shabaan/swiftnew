package com.mivors.Sjob.project.screens.EditServiceRequest;

import android.content.Context;

import com.mivors.Sjob.RightNow.RNClasses.Incident.Incident;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public interface UpdateServiceRequestPresenter {
    void setView(UpdateServiceRequestView view, Context context);
    void RequestSubmitUpdateServiceRequest(String serviceRequestID ,int payment, int severity, int visitIndex, String description, String location, String locationDetails, int contact_id, int categoryId,String lat,String lng);
    void RequestClock();
    void RequestCalender();

    void setDateAndTime(String date , String time);

    void changeVisitStatus(int position);
}
