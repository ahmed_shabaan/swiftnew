package com.mivors.Sjob.project.helpers;

/**
 * Created by Ahmed shaban on 8/1/2017.
 */

public enum  Categories {
    aircondition(27), plumbing(29),electrical(28),cleaning(96),mobile(97),satellite(98);
    private final int value;

    private Categories(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
